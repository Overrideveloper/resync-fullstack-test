# How to run
This project is set up to run in/with Docker for ease.
As such, due to the fact that it is a test project, all the environment variables have been set in the docker-compose.yml file.
Also the database migrations have been committed along with project.

All that is required to run the project:
- Have docker installed
- Have an internet connection
- Navigate to project root
- Run `docker-compose up`
- Inspect the docker run log for notification when all the containers are up
- Open `http://localhost:4200` in your browser