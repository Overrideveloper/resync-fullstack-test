/** Namespace for custom Express middleware */
declare namespace Express {
	/** Responz middleware */
	export interface Responz {
		success(data: any, message: string, code?: number, refreshAuthTokenCookie?: boolean): void;
		error(data: any, message: string, code?: number, refreshAuthTokenCookie?: boolean): void;
	}

	export interface Response {
		responz: Responz;
	}
}
