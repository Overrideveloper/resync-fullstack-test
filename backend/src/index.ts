import 'reflect-metadata';
import 'module-alias/register';
import { isMaster } from 'cluster';

import { Startup } from './startup';
import { EnvironmentConfig } from './common/environment.config';
import { USE_CLUSTER } from './constants/environment.constants';

// Configure dependency injection
Startup.configureDependencyInjection();

// Start up REST server
// Create worker cluster for REST server
if (!!parseInt(EnvironmentConfig.getEnvValue(USE_CLUSTER, 0)) === true) {
	if (isMaster) {
		Startup.spawnCluster();
	} else {
		Startup.startRestHttpServer();
	}
}
// Start up single REST server instance
else {
	Startup.startRestHttpServer();
}
