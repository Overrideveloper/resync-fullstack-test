import { MODE } from '../constants/environment.constants';
import { config } from 'dotenv';

config();

export class EnvironmentConfig {
	/**
	 * Get environment variable value for a given key
	 * @param {string} key - Environment variable key
	 * @param {any} [fallback] - Fallback value if environment variable key not found
	 * @returns {any} Environment variable value
	 */
	public static getEnvValue(key: string, fallback?: any): any {
		const value = process.env[key];

		// If value found, return value
		if (value) {
			return value;
		}
		// If value not found, return fallback value
		else if (fallback) {
			return fallback;
		}
		// If no fallback value provided, throw error
		else {
			throw new Error(`EnvironmentConfigError: Value not found for key ${key}`);
		}
	}

	/**
	 * Check if current environment is a production environment
	 * @returns {any} Environment is production or not
	 */
	public static isProdEnv(): boolean {
		return this.getEnvValue(MODE, 'dev') !== 'dev';
	}
}
