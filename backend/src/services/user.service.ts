import { MYSQL_UNIQUE_CONSTRAINT_ERROR } from '@api/constants/error.constants';
import {
	LoginRequest,
	SignupRequest,
	UserResponse,
} from '@api/DAL/DTO/user.dto';
import { User } from '@api/DAL/entity/user.entity';
import { HttpError } from '@api/DAL/models/error.model';
import { ErrorUtil } from '@api/utilities/error.util';
import { PasswordUtil } from '@api/utilities/password.util';
import { Service } from 'typedi';
import { Repository } from 'typeorm';
import jwt from 'jsonwebtoken';
import { EnvironmentConfig } from '@api/common/environment.config';
import { JWT_SECRET } from '@api/constants/environment.constants';
import { DatabaseConfig } from '@api/database/database.config';

/**
 * User Service
 */
@Service()
export class UserService {
	private repository: Repository<User>;

	constructor() {
		DatabaseConfig.getConnection().then((connection) => {
			this.repository = connection.getRepository(User);
		});
	}

	/**
	 * Signup a user
	 * @param {any} data - Signup data
	 * @returns {Promise<UserResponse>} A Promise that resolves with the created user
	 */
	public async signup(data: SignupRequest): Promise<UserResponse> {
		try {
			// Create User model instance
			const userToCreate = User.createModel({
				...data,
				password: await PasswordUtil.hashPassword(data.password),
			});

			// Save user model instance, parse to DTO and return
			return UserResponse.createFromModel(
				await this.repository.save(userToCreate),
			);
		} catch (error) {
			if (error && error.errno === MYSQL_UNIQUE_CONSTRAINT_ERROR) {
				ErrorUtil.throwDuplicateError('User already exists');
			}
			ErrorUtil.throwInternalServerError();
		}
	}

	/**
	 * Login a user using provided credentials
	 * @param {any} data - User data
	 * @returns {Promise<{ user: UserResponse; token: string }>} A Promise that resolves with the created user and authentication token
	 */
	public async login(
		data: LoginRequest,
	): Promise<{ user: UserResponse; token: string }> {
		try {
			// Find user with the provided email
			const user = await this.repository.findOne({
				where: { email: data.email },
			});

			// Throw invalid credentials error if user is not found
			if (!user) {
				ErrorUtil.throwInvalidCredentialsError();
			}

			// Compare user password and the provided password
			const isPasswordValid = await PasswordUtil.validatePassword(
				data.password,
				user.password,
			);

			// Throw invalid credentials error if password is invalid
			if (!isPasswordValid) {
				ErrorUtil.throwInvalidCredentialsError();
			}

			// Create authentication token
			const token = jwt.sign(
				user.asObject(),
				EnvironmentConfig.getEnvValue(JWT_SECRET),
			);

			return { user: UserResponse.createFromModel(user), token };
		} catch (error) {
			if (error && error instanceof HttpError) {
				throw error;
			}
			ErrorUtil.throwInternalServerError();
		}
	}

	/**
	 * Find a user using its id
	 * @param {any} id - User id
	 * @returns {Promise<UserResponse>} A Promise that resolves with the found user
	 */
	public async findById(id: string): Promise<UserResponse> {
		try {
			// Get user model instance
			const user = await this.repository.findOne(id);

			// Throw error if user is not found
			if (!user) {
				ErrorUtil.throwNotFoundError('User not found');
			}

			// Parse user to DTO and return
			return UserResponse.createFromModel(user);
		} catch (error) {
			if (error && error instanceof HttpError) {
				throw error;
			}
			ErrorUtil.throwInternalServerError();
		}
	}
}
