import { EmployeeRequest, EmployeeResponse } from '@api/DAL/DTO/employee.dto';
import { EmployeeFindConditions } from '@api/DAL/models/entity-find-conditions.model';
import { Employee } from '@api/DAL/entity/employee.entity';
import { Department } from '@api/DAL/entity/department.entity';
import { User } from '@api/DAL/entity/user.entity';
import { HttpError } from '@api/DAL/models/error.model';
import { PaginatedData } from '@api/DAL/models/paginated-data.model';
import { PaginationParams } from '@api/DAL/models/pagination-params.model';
import { ErrorUtil } from '@api/utilities/error.util';
import { Service } from 'typedi';
import { FindConditions, FindManyOptions, Repository } from 'typeorm';
import { DatabaseConfig } from '@api/database/database.config';
import { DepartmentResponse } from '@api/DAL/DTO/department.dto';

/**
 * Employee service
 */
@Service()
export class EmployeeService {
	private employeeRepository: Repository<Employee>;
	private departmentRepository: Repository<Department>;

	constructor() {
		DatabaseConfig.getConnection().then((connection) => {
			this.employeeRepository = connection.getRepository(Employee);
			this.departmentRepository = connection.getRepository(Department);
		});
	}

	/**
	 * Create an employee
	 * @param {any} data - Employee data
	 * @param {User} user - Currently authenticated user
	 * @returns {Promise<EmployeeResponse>} A Promise that resolves with the created employee
	 */
	public async create(
		data: EmployeeRequest,
		user: User,
	): Promise<EmployeeResponse> {
		try {
			// Get department to create employee for
			const department = await this.departmentRepository.findOne(
				data.departmentId,
			);

			// Throw error if department is not found
			if (!department) {
				ErrorUtil.throwNotFoundError('Department not found');
			}

			// Create Employee model instance
			const employeeToCreate = Employee.createModel({
				...data,
				department,
				createdBy: user,
			});

			// Save employee model instance, parse to DTO and return
			return EmployeeResponse.createFromModel(
				await this.employeeRepository.save(employeeToCreate),
			);
		} catch (error) {
			if (error && error instanceof HttpError) {
				throw error;
			}
			ErrorUtil.throwInternalServerError();
		}
	}

	/**
	 * Update an employee
	 * @param {any} id - Employee id
	 * @param {any} data - Employee data
	 * @param {User} user - Currently authenticated user
	 * @returns {Promise<EmployeeResponse>} A Promise that resolves with the updated employee
	 */
	public async update(
		id: string,
		data: EmployeeRequest,
		user: User,
	): Promise<EmployeeResponse> {
		try {
			// Get department to update employee with
			const department = await this.departmentRepository.findOne(
				data.departmentId,
				{
					where: { createdBy: { id: user.id } },
					relations: ['organization'],
				},
			);

			// Throw error if department is not found
			if (!department) {
				ErrorUtil.throwNotFoundError('Department not found');
			}

			// Get employee model instance to update
			const employeeToUpdate = await this.employeeRepository.findOne(id, {
				where: { createdBy: { id: user.id } },
				relations: ['department', 'department.organization'],
			});

			// Throw error if employee is not found
			if (!employeeToUpdate) {
				ErrorUtil.throwNotFoundError('Employee not found');
			}

			// Throw error if department organization is different from employeeToUpdate organization
			// Organization cannot be updated
			if (
				department.organization.id !==
				employeeToUpdate.department.organization.id
			) {
				ErrorUtil.throwBadRequestError('Invalid department provided.');
			}

			// Update Employee model instance
			delete data.departmentId;
			employeeToUpdate.updateModel({
				...data,
				department,
			});

			// Save employee model instance, parse to DTO and return
			const employeeDTO = EmployeeResponse.createFromModel(
				await this.employeeRepository.save(employeeToUpdate),
			);
			employeeDTO.department = DepartmentResponse.createFromModel(department);

			return employeeDTO;
		} catch (error) {
			if (error && error instanceof HttpError) {
				throw error;
			}
			ErrorUtil.throwInternalServerError();
		}
	}

	/**
	 * Delete an employee
	 * @param {any} id - Employee id
	 * @param {User} user - Currently authenticated user
	 * @returns {Promise<void>} An empty Promise
	 */
	public async delete(id: string, user: User): Promise<void> {
		try {
			// Get employee model instance to delete
			const employeeToDelete = await this.employeeRepository.findOne(id, {
				where: { createdBy: { id: user.id } },
			});

			// Throw error if employee is not found
			if (!employeeToDelete) {
				ErrorUtil.throwNotFoundError('Employee not found');
			}

			await this.employeeRepository.remove(employeeToDelete);
			return;
		} catch (error) {
			if (error && error instanceof HttpError) {
				throw error;
			}
			ErrorUtil.throwInternalServerError();
		}
	}

	/**
	 * Find an employee using its id
	 * @param {any} id - Employee id
	 * @param {User} user - Currently authenticated user
	 * @returns {Promise<EmployeeResponse>} A Promise that resolves with the found employee
	 */
	public async findById(id: string, user: User): Promise<EmployeeResponse> {
		try {
			// Get employee model instance
			const employee = await this.employeeRepository.findOne(id, {
				where: { createdBy: { id: user.id } },
				relations: ['department', 'department.organization'],
			});

			// Throw error if employee is not found
			if (!employee) {
				ErrorUtil.throwNotFoundError('Employee not found');
			}

			// Parse employee to DTO and return
			return EmployeeResponse.createFromModel(employee);
		} catch (error) {
			if (error && error instanceof HttpError) {
				throw error;
			}
			ErrorUtil.throwInternalServerError();
		}
	}

	/**
	 * Find multiple employees
	 * @param {User} user - Currently authenticated user
	 * @param {EmployeeFindConditions} [conditions] - Conditions to use in finding the employees
	 * @returns {Promise<EmployeeResponse[]>} A Promise that resolves with a list of found employees
	 */
	public async findMany(
		user: User,
		conditions?: EmployeeFindConditions,
	): Promise<EmployeeResponse[]> {
		try {
			// Get only employees created by the current user
			conditions.createdById = user.id;

			// Get employee model instance list
			const employeeList = await this.employeeRepository.find({
				where: this.normalizeFindConditions(conditions),
				relations: ['department', 'department.organization'],
			});

			// Parse employee list to DTO and return
			return employeeList.map((employee) =>
				EmployeeResponse.createFromModel(employee),
			);
		} catch (_) {
			ErrorUtil.throwInternalServerError();
		}
	}

	/**
	 * Find multiple employees and paginate the results
	 * @param {User} user - Currently authenticated user
	 * @param {PaginationParams} paginationParams - Pagination information
	 * @param {EmployeeFindConditions} [conditions] - Conditions to use in finding the employees
	 * @returns {Promise<PaginatedData<EmployeeResponse>>} A Promise that resolves with a paginated list of found employees
	 */
	public async findManyPaginated(
		user: User,
		paginationParams: PaginationParams,
		conditions?: EmployeeFindConditions,
	): Promise<PaginatedData<EmployeeResponse>> {
		try {
			// Get only employees created by the current user
			conditions.createdById = user.id;

			// Paginate returned employee model instances
			const findOptions: FindManyOptions<Employee> = {
				take: paginationParams.perPage,
				skip: paginationParams.offset,
				where: this.normalizeFindConditions(conditions),
				relations: ['department', 'department.organization'],
			};

			// Get employee model instances
			const employeeListAndCount = await this.employeeRepository.findAndCount(
				findOptions,
			);
			const totalEmployeeCount = employeeListAndCount[1];

			// Parse employee list to DTO
			const employeeListDTO = employeeListAndCount[0].map((employee) =>
				EmployeeResponse.createFromModel(employee),
			);

			// Parse employee list to DTO and return
			return PaginatedData.createFromPaginatedRecords<EmployeeResponse>({
				data: employeeListDTO,
				page: paginationParams.page,
				pageCount: Math.ceil(totalEmployeeCount / paginationParams.perPage),
				pageSize: paginationParams.perPage,
				totalDataCount: totalEmployeeCount,
			});
		} catch (_) {
			ErrorUtil.throwInternalServerError();
		}
	}

	/**
	 * Normalize employee find conditions
	 * @param {EmployeeFindConditions} conditions - Employee find conditinos
	 * @returns {FindConditions<Employee>} Normalized employee find conditions
	 */
	private normalizeFindConditions(
		conditions: EmployeeFindConditions,
	): FindConditions<Employee> {
		let normalizedConditions = {};

		if (conditions) {
			normalizedConditions = Object.entries(conditions).reduce<
				FindConditions<Employee>
			>(
				(
					conditionMap,
					[key, value]: [keyof EmployeeRequest | 'createdById', any],
				) => {
					// Normalize createdById property by passing it to the id property in the createdBy FindCondition
					if (key === 'createdById') {
						conditionMap.createdBy = { id: value as string };
					}
					// Normalize organizationId property by passing it to the id property in the organization FindCondition
					else if (key === 'departmentId') {
						conditionMap.department = { id: value as string };
					}
					// Add other properties only if they are valid employee properties
					else if (Object.keys(EmployeeRequest).includes(key)) {
						conditionMap[key] = value;
					}
					return conditionMap;
				},
				{},
			);
		}

		return normalizedConditions;
	}
}
