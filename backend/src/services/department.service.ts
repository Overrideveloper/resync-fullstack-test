import {
	DepartmentRequest,
	DepartmentResponse,
} from '@api/DAL/DTO/department.dto';
import { DepartmentFindConditions } from '@api/DAL/models/entity-find-conditions.model';
import { Department } from '@api/DAL/entity/department.entity';
import { Organization } from '@api/DAL/entity/organization.entity';
import { User } from '@api/DAL/entity/user.entity';
import { HttpError } from '@api/DAL/models/error.model';
import { PaginatedData } from '@api/DAL/models/paginated-data.model';
import { PaginationParams } from '@api/DAL/models/pagination-params.model';
import { ErrorUtil } from '@api/utilities/error.util';
import { Service } from 'typedi';
import { FindConditions, FindManyOptions, Repository } from 'typeorm';
import { DatabaseConfig } from '@api/database/database.config';
import { OrganizationResponse } from '@api/DAL/DTO/organization.dto';

/**
 * Department service
 */
@Service()
export class DepartmentService {
	private departmentRepository: Repository<Department>;
	private organizationRepository: Repository<Organization>;

	constructor() {
		DatabaseConfig.getConnection().then((connection) => {
			this.departmentRepository = connection.getRepository(Department);
			this.organizationRepository = connection.getRepository(Organization);
		});
	}

	/**
	 * Create a department
	 * @param {any} data - Department data
	 * @param {User} user - Currently authenticated user
	 * @returns {Promise<DepartmentResponse>} A Promise that resolves with the created department
	 */
	public async create(
		data: DepartmentRequest,
		user: User,
	): Promise<DepartmentResponse> {
		try {
			// Get organization to create department for
			const organization = await this.organizationRepository.findOne(
				data.organizationId,
			);

			// Throw error if organization is not found
			if (!organization) {
				ErrorUtil.throwNotFoundError('Organization not found');
			}

			// Create Department model instance
			const departmentToCreate = Department.createModel({
				...data,
				organization,
				createdBy: user,
			});

			// Save department model instance, parse to DTO and return
			return DepartmentResponse.createFromModel(
				await this.departmentRepository.save(departmentToCreate),
			);
		} catch (error) {
			if (error && error instanceof HttpError) {
				throw error;
			}
			ErrorUtil.throwInternalServerError();
		}
	}

	/**
	 * Update a department
	 * @param {any} id - Department id
	 * @param {any} data - Department data
	 * @param {User} user - Currently authenticated user
	 * @returns {Promise<DepartmentResponse>} A Promise that resolves with the updated department
	 */
	public async update(
		id: string,
		data: DepartmentRequest,
		user: User,
	): Promise<DepartmentResponse> {
		try {
			// Remove organization ID property
			// Organization property cannot be updated
			delete data.organizationId;

			// Get department model instance to update
			const departmentToUpdate = await this.departmentRepository.findOne(id, {
				where: { createdBy: { id: user.id } },
				relations: ['organization'],
			});

			// Throw error if department is not found
			if (!departmentToUpdate) {
				ErrorUtil.throwNotFoundError('Department not found');
			}

			// Update Department model instance
			departmentToUpdate.updateModel(data);

			// Save department model instance, parse to DTO and return
			const departmentDTO = DepartmentResponse.createFromModel(
				await this.departmentRepository.save(departmentToUpdate),
			);
      departmentDTO.organization = OrganizationResponse.createFromModel(departmentToUpdate.organization);

      return departmentDTO;
		} catch (error) {
			if (error && error instanceof HttpError) {
				throw error;
			}
			ErrorUtil.throwInternalServerError();
		}
	}

	/**
	 * Delete a department
	 * @param {any} id - Department id
	 * @param {User} user - Currently authenticated user
	 * @returns {Promise<void>} An empty Promise
	 */
	public async delete(id: string, user: User): Promise<void> {
		try {
			// Get department model instance to delete
			const departmentToDelete = await this.departmentRepository.findOne(id, {
				where: { createdBy: { id: user.id } },
			});

			// Throw error if department is not found
			if (!departmentToDelete) {
				ErrorUtil.throwNotFoundError('Department not found');
			}

			await this.departmentRepository.remove(departmentToDelete);
			return;
		} catch (error) {
			if (error && error instanceof HttpError) {
				throw error;
			}
			ErrorUtil.throwInternalServerError();
		}
	}

	/**
	 * Find a department using its id
	 * @param {any} id - Department id
	 * @param {User} user - Currently authenticated user
	 * @returns {Promise<DepartmentResponse>} A Promise that resolves with the found department
	 */
	public async findById(id: string, user: User): Promise<DepartmentResponse> {
		try {
			// Get department model instance
			const department = await this.departmentRepository.findOne(id, {
				where: { createdBy: { id: user.id } },
				relations: ['organization'],
			});

			// Throw error if department is not found
			if (!department) {
				ErrorUtil.throwNotFoundError('Department not found');
			}

			// Parse department to DTO and return
			return DepartmentResponse.createFromModel(department);
		} catch (error) {
			if (error && error instanceof HttpError) {
				throw error;
			}
			ErrorUtil.throwInternalServerError();
		}
	}

	/**
	 * Find multiple departments
	 * @param {User} user - Currently authenticated user
	 * @param {DepartmentFindConditions} [conditions] - Conditions to use in finding the departments
	 * @returns {Promise<DepartmentResponse[]>} A Promise that resolves with a list of found departments
	 */
	public async findMany(
		user: User,
		conditions = {} as DepartmentFindConditions,
	): Promise<DepartmentResponse[]> {
		try {
			// Get only departments created by the current user
			conditions.createdById = user.id;

			// Get department model instance list
			const departmentList = await this.departmentRepository.find({
				where: this.normalizeFindConditions(conditions),
				relations: ['organization'],
			});

			// Parse department list to DTO and return
			return departmentList.map((department) =>
				DepartmentResponse.createFromModel(department),
			);
		} catch (_) {
			ErrorUtil.throwInternalServerError();
		}
	}

	/**
	 * Find multiple departments and paginate the results
	 * @param {User} user - Currently authenticated user
	 * @param {PaginationParams} paginationParams - Pagination information
	 * @param {DepartmentFindConditions} [conditions] - Conditions to use in finding the departments
	 * @returns {Promise<PaginatedData<DepartmentResponse>>} A Promise that resolves with a paginated list of found departments
	 */
	public async findManyPaginated(
		user: User,
		paginationParams: PaginationParams,
		conditions = {} as DepartmentFindConditions,
	): Promise<PaginatedData<DepartmentResponse>> {
		try {
			// Get only departments created by the current user
			conditions.createdById = user.id;

			// Paginate returned department model instances
			const findOptions: FindManyOptions<Department> = {
				take: paginationParams.perPage,
				skip: paginationParams.offset,
				where: this.normalizeFindConditions(conditions),
				relations: ['organization'],
			};

			// Get department model instances
			const departmentListAndCount = await this.departmentRepository.findAndCount(
				findOptions,
			);
			const totalDepartmentCount = departmentListAndCount[1];

			// Parse department list to DTO
			const departmentListDTO = departmentListAndCount[0].map((department) =>
				DepartmentResponse.createFromModel(department),
			);

			// Parse department list to DTO and return
			return PaginatedData.createFromPaginatedRecords<DepartmentResponse>({
				data: departmentListDTO,
				page: paginationParams.page,
				pageCount: Math.ceil(totalDepartmentCount / paginationParams.perPage),
				pageSize: paginationParams.perPage,
				totalDataCount: totalDepartmentCount,
			});
		} catch (_) {
			ErrorUtil.throwInternalServerError();
		}
	}

	/**
	 * Normalize department find conditions
	 * @param {DepartmentFindConditions} conditions - Department find conditinos
	 * @returns {FindConditions<Department>} Normalized deparment find conditions
	 */
	private normalizeFindConditions(
		conditions: DepartmentFindConditions,
	): FindConditions<Department> {
		let normalizedConditions = {};

		if (conditions) {
			normalizedConditions = Object.entries(conditions).reduce<
				FindConditions<Department>
			>(
				(
					conditionMap,
					[key, value]: [keyof DepartmentRequest | 'createdById', any],
				) => {
					// Normalize createdById property by passing it to the id property in the createdBy FindCondition
					if (key === 'createdById') {
						conditionMap.createdBy = { id: value as string };
					}
					// Normalize organizationId property by passing it to the id property in the organization FindCondition
					else if (key === 'organizationId') {
						conditionMap.organization = { id: value as string };
					}
					// Add other properties only if they are valid department properties
					else if (Object.keys(DepartmentRequest).includes(key)) {
						conditionMap[key] = value;
					}
					return conditionMap;
				},
				{},
			);
		}

		return normalizedConditions;
	}
}
