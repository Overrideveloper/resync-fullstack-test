import { Department } from '@api/DAL/entity/department.entity';
import { Employee } from '@api/DAL/entity/employee.entity';
import { Organization } from '@api/DAL/entity/organization.entity';
import { User } from '@api/DAL/entity/user.entity';
import { DatabaseConfig } from '@api/database/database.config';
import { ErrorUtil } from '@api/utilities/error.util';
import { Service } from 'typedi';
import { FindManyOptions, Repository } from 'typeorm';

interface EntityCounts {
	organization: number;
	department: number;
	employee: number;
}

/**
 * Insights Service
 */
@Service()
export class InsightsService {
	private organizationRepository: Repository<Organization>;
	private departmentRepository: Repository<Department>;
	private employeeRepository: Repository<Employee>;

	constructor() {
		DatabaseConfig.getConnection().then((connection) => {
			this.organizationRepository = connection.getRepository(Organization);
			this.departmentRepository = connection.getRepository(Department);
			this.employeeRepository = connection.getRepository(Employee);
		});
	}

	/**
	 * Get entity counts
	 *
	 * Get counts of Organizations, Departments and Employees created by the current user
	 * @param {User} user - Currently authenticated user
	 * @returns {Promise<EntityCounts>} A Promise that resolves with the entity counts
	 */
	public async getEntitiesCount(user: User): Promise<EntityCounts> {
		try {
			// Get only entities created by the current user
			const findOptions: FindManyOptions<
				Organization | Department | Employee
			> = {
				where: { createdBy: { id: user.id } },
			};

			// Get entity counts
			const countList = await Promise.all([
				this.organizationRepository.count(findOptions),
				this.departmentRepository.count(findOptions),
				this.employeeRepository.count(findOptions),
			]);

			// Return entity counts
			return {
				organization: countList[0],
				department: countList[1],
				employee: countList[2],
			};
		} catch (_) {
			ErrorUtil.throwInternalServerError();
		}
	}
}
