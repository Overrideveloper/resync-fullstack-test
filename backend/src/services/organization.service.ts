import {
	OrganizationRequest,
	OrganizationResponse,
} from '@api/DAL/DTO/organization.dto';
import { OrganizationFindConditions } from '@api/DAL/models/entity-find-conditions.model';
import { Organization } from '@api/DAL/entity/organization.entity';
import { User } from '@api/DAL/entity/user.entity';
import { HttpError, RecordNotFoundError } from '@api/DAL/models/error.model';
import { PaginatedData } from '@api/DAL/models/paginated-data.model';
import { PaginationParams } from '@api/DAL/models/pagination-params.model';
import { ErrorUtil } from '@api/utilities/error.util';
import { Service } from 'typedi';
import { FindConditions, FindManyOptions, Repository } from 'typeorm';
import { DatabaseConfig } from '@api/database/database.config';

/**
 * Organization service
 */
@Service()
export class OrganizationService {
	private repository: Repository<Organization>;

	constructor() {
		DatabaseConfig.getConnection().then((connection) => {
			this.repository = connection.getRepository(Organization);
		});
	}

	/**
	 * Create an organization
	 * @param {any} data - Organization data
	 * @param {User} user - Currently authenticated user
	 * @returns {Promise<OrganizationResponse>} A Promise that resolves with the created organization
	 */
	public async create(
		data: OrganizationRequest,
		user: User,
	): Promise<OrganizationResponse> {
		try {
			// Create Organization model instance
			const organizationToCreate = Organization.createModel({
				...data,
				createdBy: user,
			});

			// Save organization model instance, parse to DTO and return
			return OrganizationResponse.createFromModel(
				await this.repository.save(organizationToCreate),
			);
		} catch (_) {
			ErrorUtil.throwInternalServerError();
		}
	}

	/**
	 * Update an organization
	 * @param {any} id - Organization id
	 * @param {any} data - Organization data
	 * @param {User} user - Currently authenticated user
	 * @returns {Promise<OrganizationResponse>} A Promise that resolves with the updated organization
	 */
	public async update(
		id: string,
		data: OrganizationRequest,
		user: User,
	): Promise<OrganizationResponse> {
		try {
			// Get organization model instance to update
			const organizationToUpdate = await this.repository.findOne(id, {
				where: { createdBy: { id: user.id } },
			});

			// Throw error if organization is not found
			if (!organizationToUpdate) {
				ErrorUtil.throwNotFoundError('Organization not found');
			}

			// Update Organization model instance
			organizationToUpdate.updateModel(data);

			// Save organization model instance, parse to DTO and return
			return OrganizationResponse.createFromModel(
				await this.repository.save(organizationToUpdate),
			);
		} catch (error) {
			if (error && error instanceof HttpError) {
				throw error;
			}
			ErrorUtil.throwInternalServerError();
		}
	}

	/**
	 * Delete an organization
	 * @param {any} id - Organization id
	 * @param {User} user - Currently authenticated user
	 * @returns {Promise<void>} An empty Promise
	 */
	public async delete(id: string, user: User): Promise<void> {
		try {
			// Get organization model instance to delete
			const organizationToDelete = await this.repository.findOne(id, {
				where: { createdBy: { id: user.id } },
			});

			// Throw error if organization is not found
			if (!organizationToDelete) {
				ErrorUtil.throwNotFoundError('Organization not found');
			}

			await this.repository.remove(organizationToDelete);
			return;
		} catch (error) {
			if (error && error instanceof HttpError) {
				throw error;
			}
			ErrorUtil.throwInternalServerError();
		}
	}

	/**
	 * Find an organization using its id
	 * @param {any} id - Organization id
	 * @param {User} user - Currently authenticated user
	 * @returns {Promise<OrganizationResponse>} A Promise that resolves with the found organization
	 */
	public async findById(id: string, user: User): Promise<OrganizationResponse> {
		try {
			// Get organization model instance
			const organization = await this.repository.findOne(id, {
				where: { createdBy: { id: user.id } },
			});

			// Throw error if organization is not found
			if (!organization) {
				ErrorUtil.throwNotFoundError('Organization not found');
			}

			// Parse organization to DTO and return
			return OrganizationResponse.createFromModel(organization);
		} catch (error) {
			if (error && error instanceof HttpError) {
				throw error;
			}
			ErrorUtil.throwInternalServerError();
		}
	}

	/**
	 * Find multiple organizations
	 * @param {User} user - Currently authenticated user
	 * @param {OrganizationFindConditions} [conditions] - Conditions to use in finding the organizations
	 * @returns {Promise<OrganizationResponse[]>} A Promise that resolves with a list of found organizations
	 */
	public async findMany(
		user: User,
		conditions?: OrganizationFindConditions,
	): Promise<OrganizationResponse[]> {
		try {
			// Get only organizations created by the current user
			conditions.createdById = user.id;

			// Get organization model instance list
			const organizationList = await this.repository.find({
				where: this.normalizeFindConditions(conditions),
			});

			// Parse organization list to DTO and return
			return organizationList.map((organization) =>
				OrganizationResponse.createFromModel(organization),
			);
		} catch (_) {
			ErrorUtil.throwInternalServerError();
		}
	}

	/**
	 * Find multiple organizations and paginate the results
	 * @param {User} user - Currently authenticated user
	 * @param {PaginationParams} paginationParams - Pagination information
	 * @param {OrganizationFindConditions} [conditions] - Conditions to use in finding the organizations
	 * @returns {Promise<PaginatedData<OrganizationResponse>>} A Promise that resolves with a paginated list of found organizations
	 */
	public async findManyPaginated(
		user: User,
		paginationParams: PaginationParams,
		conditions?: OrganizationFindConditions,
	): Promise<PaginatedData<OrganizationResponse>> {
		try {
			// Get only organizations created by the current user
			conditions.createdById = user.id;

			// Paginate returned organizations model instances
			const findOptions: FindManyOptions<Organization> = {
				take: paginationParams.perPage,
				skip: paginationParams.offset,
				where: this.normalizeFindConditions(conditions),
			};

			// Get organization model instances
			const organizationListAndCount = await this.repository.findAndCount(
				findOptions,
			);
			const totalOrganizationCount = organizationListAndCount[1];

			// Parse organization list to DTO
			const organizationListDTO = organizationListAndCount[0].map(
				(organization) => OrganizationResponse.createFromModel(organization),
			);

			// Parse organization list to DTO and return
			return PaginatedData.createFromPaginatedRecords<OrganizationResponse>({
				data: organizationListDTO,
				page: paginationParams.page,
				pageCount: Math.ceil(totalOrganizationCount / paginationParams.perPage),
				pageSize: paginationParams.perPage,
				totalDataCount: totalOrganizationCount,
			});
		} catch (_) {
			ErrorUtil.throwInternalServerError();
		}
	}

	/**
	 * Normalize organization find conditions
	 * @param {OrganizationFindConditions} conditions - Organization find conditinos
	 * @returns {FindConditions<Organization>} Normalized organization find conditions
	 */
	private normalizeFindConditions(
		conditions: OrganizationFindConditions,
	): FindConditions<Organization> {
		let normalizedConditions = {};

		if (conditions) {
			normalizedConditions = Object.entries(conditions).reduce<
				FindConditions<Organization>
			>(
				(
					conditionMap,
					[key, value]: [keyof OrganizationRequest | 'createdById', any],
				) => {
					// Normalize createdById property by passing it to the id property in the createdBy FindCondition
					if (key === 'createdById') {
						conditionMap.createdBy = { id: value as string };
					}
					// Add other properties only if they are valid organization properties
					else if (Object.keys(OrganizationRequest).includes(key)) {
						conditionMap[key] = value;
					}
					return conditionMap;
				},
				{},
			);
		}

		return normalizedConditions;
	}
}
