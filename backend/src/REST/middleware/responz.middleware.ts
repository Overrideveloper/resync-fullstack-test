import { COOKIE_KEY, COOKIE_OPTIONS } from '@api/constants/cookie.constants';
import { Response, Request, NextFunction } from 'express';
import { INTERNAL_SERVER_ERROR, OK } from 'http-status-codes';

/**
 * Responz Middleware
 * @implements {Express.Responz}
 */
class ResponzMiddleware implements Express.Responz {
	constructor(private readonly req: Request, private readonly res: Response) {}

	/**
	 * Handle success response
	 * @param {any} data - Response Data
	 * @param {string} message - Response message
	 * @param {number} [code] - Response code
	 * @param {boolean} [refreshAuthTokenCookie] - Refresh auth token
	 */
	public success(
		data: any,
		message: string,
		code: number = OK,
		refreshAuthTokenCookie = true,
	) {
		if (refreshAuthTokenCookie === true) {
			this.refreshAuthTokenCookieIfPresent();
		}
		this.res.status(code).json({ data, message, code });
	}

	/**
	 * Handle error response
	 * @param {any} data - Response Data
	 * @param {string} message - Response message
	 * @param {number} [code] - Response code
	 * @param {boolean} [refreshAuthTokenCookie] - Refresh auth token
	 */
	public error(
		data: any,
		message: string,
		code: number = INTERNAL_SERVER_ERROR,
		refreshAuthTokenCookie = true,
	) {
		if (refreshAuthTokenCookie === true) {
			this.refreshAuthTokenCookieIfPresent();
		}
		this.res.status(code).json({ data, message, code });
	}

	/**
	 * Refresh authorization token cookie if present
	 *
	 * This increments the expiry of the authorization token for every authenticated request made.
	 * This prevents the authorization token from expiring until user logs out
	 */
	public refreshAuthTokenCookieIfPresent(): void {
		const token: string = this.req.cookies[COOKIE_KEY];

		if (token) {
			this.res.cookie(COOKIE_KEY, token, COOKIE_OPTIONS);
		}
	}
}

export default function (req: Request, res: Response, next: NextFunction) {
	res.responz = new ResponzMiddleware(req, res);
	next();
}
