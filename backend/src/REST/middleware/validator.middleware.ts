import { AnySchema } from 'joi';
import { Response, Request, NextFunction } from 'express';
import StatusCode from 'http-status-codes';
import { ValidatorMiddlewareUtil } from '@api/utilities/validator-middleware.util';

export class ValidatorMiddleware {
	/**
	 * Validate Request Body
	 * @param {AnySchema} schema - Validation schema
	 */
	public static validateBody(schema: AnySchema) {
		return function (req: Request, res: Response, next: NextFunction) {
			const { err, value } = ValidatorMiddlewareUtil.validateReqData(
				req.body,
				schema,
			);

			if (err) {
				return res.responz.error(
					err,
					'Validation errors occured',
					StatusCode.BAD_REQUEST,
				);
			}

			req.body = value;
			return next();
		};
	}

	/**
	 * Validate Request Query
	 * @param {AnySchema} schema - Validation schema
	 */
	public static validateQuery(schema: AnySchema) {
		return function (req: Request, res: Response, next: NextFunction) {
			const { err, value } = ValidatorMiddlewareUtil.validateReqData(
				req.query,
				schema,
			);

			if (err) {
				return res.responz.error(
					err,
					'Validation errors occured',
					StatusCode.BAD_REQUEST,
				);
			}

			req.query = value;
			return next();
		};
	}

	/**
	 * Validate Request Params
	 * @param {AnySchema} schema - Validation schema
	 */
	public static validateParams(schema: AnySchema) {
		return function (req: Request, res: Response, next: NextFunction) {
			const { err, value } = ValidatorMiddlewareUtil.validateReqData(
				req.params,
				schema,
			);

			if (err) {
				return res.responz.error(
					err,
					'Validation errors occured',
					StatusCode.BAD_REQUEST,
				);
			}

			req.params = value;
			return next();
		};
	}
}
