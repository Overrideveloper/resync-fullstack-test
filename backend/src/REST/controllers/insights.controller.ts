import { Service } from 'typedi';
import { Request, Response } from 'express';
import StatusCode from 'http-status-codes';
import { User } from '@api/DAL/entity/user.entity';
import { InsightsService } from '@api/services/insights.service';
import { BaseController } from './base.controller';

/**
 * Insights Controller
 * @extends BaseController
 */
@Service()
export class InsightsController extends BaseController {
	constructor(private insightsService: InsightsService) {
		super();
	}

	/**
	 * Process "get entity count" request
	 * @param {Express.Request} req - Request
	 * @param {Express.Response} res - Response
	 */
	public async getEntitiesCount(req: Request, res: Response) {
		try {
			const data = await this.insightsService.getEntitiesCount(
				req.user as User,
			);
			this.handleSuccess(res, data, 'Insights returned', StatusCode.OK);
		} catch (err) {
			this.handleError(res, err);
		}
	}
}
