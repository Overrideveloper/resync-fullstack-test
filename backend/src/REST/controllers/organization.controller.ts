import { Service } from 'typedi';
import { Request, Response } from 'express';
import StatusCode from 'http-status-codes';
import { OrganizationService } from '@api/services/organization.service';
import { BaseController } from './base.controller';
import {
	OrganizationRequest,
	OrganizationResponse,
} from '@api/DAL/DTO/organization.dto';
import { User } from '@api/DAL/entity/user.entity';
import { PaginationUtil } from '@api/utilities/pagination.util';
import { QueryParams } from '@api/DAL/models/query-params.model';
import { PaginatedData } from '@api/DAL/models/paginated-data.model';
import { OrganizationFindConditions } from '@api/DAL/models/entity-find-conditions.model';

/**
 * Organization Controller
 * @extends BaseController
 */
@Service()
export class OrganizationController extends BaseController {
	constructor(private organizationService: OrganizationService) {
		super();
	}

	/**
	 * Process create request
	 * @param {Express.Request} req - Request
	 * @param {Express.Response} res - Response
	 */
	public async create(req: Request, res: Response) {
		try {
			const data = await this.organizationService.create(
				OrganizationRequest.createFromRequest(req.body),
				req.user as User,
			);
			this.handleSuccess(res, data, 'Organization created', StatusCode.CREATED);
		} catch (err) {
			this.handleError(res, err);
		}
	}

	/**
	 * Process update request
	 * @param {Express.Request} req - Request
	 * @param {Express.Response} res - Response
	 */
	public async update(req: Request, res: Response) {
		try {
			const data = await this.organizationService.update(
				req.params.id,
				OrganizationRequest.createFromRequest(req.body),
				req.user as User,
			);
			this.handleSuccess(res, data, 'Organization updated', StatusCode.OK);
		} catch (err) {
			this.handleError(res, err);
		}
	}

	/**
	 * Process delete request
	 * @param {Express.Request} req - Request
	 * @param {Express.Response} res - Response
	 */
	public async delete(req: Request, res: Response) {
		try {
			await this.organizationService.delete(req.params.id, req.user as User);
			this.handleSuccess(res, null, 'Organization deleted', StatusCode.OK);
		} catch (err) {
			this.handleError(res, err);
		}
	}

	/**
	 * Process "get one" request
	 * @param {Express.Request} req - Request
	 * @param {Express.Response} res - Response
	 */
	public async getOne(req: Request, res: Response) {
		try {
			const data = await this.organizationService.findById(
				req.params.id,
				req.user as User,
			);
			this.handleSuccess(res, data, 'Organization found', StatusCode.OK);
		} catch (err) {
			this.handleError(res, err);
		}
	}

	/**
	 * Process "get many" request
	 * @param {Express.Request} req - Request
	 * @param {Express.Response} res - Response
	 */
	public async getMany(req: Request, res: Response) {
		try {
			// Get query params
			const queryParams = QueryParams.createFromRequest<OrganizationFindConditions>(
				req.query,
			);
			// Get pagination params from query params
			const paginationParams = PaginationUtil.getParamsFromQueryParams(
				queryParams,
			);

			let data: OrganizationResponse[] | PaginatedData<OrganizationResponse>;
			let dataCount: number;

			// Get paginated data if pagination params were provided
			if (paginationParams) {
				data = await this.organizationService.findManyPaginated(
					req.user as User,
					paginationParams,
					queryParams.params,
				);
				dataCount = data.data.length;
			}
			// Get un-paginated data
			else {
				data = await this.organizationService.findMany(
					req.user as User,
					queryParams.params,
				);
				dataCount = data.length;
			}

			this.handleSuccess(
				res,
				data,
				`${dataCount} organizations returned`,
				StatusCode.OK,
			);
		} catch (err) {
			console.error(err);
			this.handleError(res, err);
		}
	}
}
