import { Request, Response } from 'express';
import StatusCode, { OK } from 'http-status-codes';
import { LoginRequest, SignupRequest } from '@api/DAL/DTO/user.dto';
import { UserService } from '@api/services/user.service';
import { Service } from 'typedi';
import { BaseController } from './base.controller';
import { COOKIE_KEY, COOKIE_OPTIONS } from '@api/constants/cookie.constants';
import { User } from '@api/DAL/entity/user.entity';

/**
 * User Controller
 * @extends BaseController
 */
@Service()
export class UserController extends BaseController {
	constructor(private userService: UserService) {
		super();
	}

	/**
	 * Process signup request
	 * @param {Express.Request} req - Request
	 * @param {Express.Response} res - Response
	 */
	public async signup(req: Request, res: Response) {
		try {
			const data = await this.userService.signup(
				SignupRequest.createFromRequest(req.body),
			);
			this.handleSuccess(res, data, 'Signup successful', StatusCode.CREATED);
		} catch (err) {
			this.handleError(res, err);
		}
	}

	/**
	 * Process login request
	 * @param {Express.Request} req - Request
	 * @param {Express.Response} res - Response
	 */
	public async login(req: Request, res: Response) {
		try {
			const { user, token } = await this.userService.login(
				LoginRequest.createFromRequest(req.body),
			);

			// Set token in response cookie
			res.cookie(COOKIE_KEY, token, COOKIE_OPTIONS);

			this.handleSuccess(res, user, 'Login successful');
		} catch (err) {
			this.handleError(res, err);
		}
	}

	/**
	 * Process "get current user" request
	 * @param {Express.Request} req - Request
	 * @param {Express.Response} res - Response
	 */
	public async getCurrentUser(req: Request, res: Response) {
		try {
			const user = this.userService.findById((req.user as User).id);
			this.handleSuccess(res, user, 'User returned');
		} catch (err) {
			this.handleError(res, err);
		}
	}

	/**
	 * Process "logout" request
	 * @param {Express.Request} req - Request
	 * @param {Express.Response} res - Response
	 */
	public async logoutCurrentUser(req: Request, res: Response) {
		try {
			// Destroy cookie
			res.cookie(COOKIE_KEY, req.cookies[COOKIE_KEY], {
				...COOKIE_OPTIONS,
				maxAge: 0,
			});
			this.handleSuccess(res, null, 'User logged out', OK, false);
		} catch (err) {
			this.handleError(res, err);
		}
	}
}
