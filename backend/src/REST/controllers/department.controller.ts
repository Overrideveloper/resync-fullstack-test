import { Service } from 'typedi';
import { Request, Response } from 'express';
import StatusCode from 'http-status-codes';
import { DepartmentService } from '@api/services/department.service';
import { BaseController } from './base.controller';
import {
	DepartmentRequest,
	DepartmentResponse,
} from '@api/DAL/DTO/department.dto';
import { User } from '@api/DAL/entity/user.entity';
import { PaginationUtil } from '@api/utilities/pagination.util';
import { QueryParams } from '@api/DAL/models/query-params.model';
import { PaginatedData } from '@api/DAL/models/paginated-data.model';
import { DepartmentFindConditions } from '@api/DAL/models/entity-find-conditions.model';

/**
 * Department Controller
 * @extends BaseController
 */
@Service()
export class DepartmentController extends BaseController {
	constructor(private departmentService: DepartmentService) {
		super();
	}

	/**
	 * Process create request
	 * @param {Express.Request} req - Request
	 * @param {Express.Response} res - Response
	 */
	public async create(req: Request, res: Response) {
		try {
			const data = await this.departmentService.create(
				DepartmentRequest.createFromRequest(req.body),
				req.user as User,
			);
			this.handleSuccess(res, data, 'Department created', StatusCode.CREATED);
		} catch (err) {
			this.handleError(res, err);
		}
	}

	/**
	 * Process update request
	 * @param {Express.Request} req - Request
	 * @param {Express.Response} res - Response
	 */
	public async update(req: Request, res: Response) {
		try {
			const data = await this.departmentService.update(
				req.params.id,
				DepartmentRequest.createFromRequest(req.body),
				req.user as User,
			);
			this.handleSuccess(res, data, 'Department updated', StatusCode.OK);
		} catch (err) {
			this.handleError(res, err);
		}
	}

	/**
	 * Process delete request
	 * @param {Express.Request} req - Request
	 * @param {Express.Response} res - Response
	 */
	public async delete(req: Request, res: Response) {
		try {
			await this.departmentService.delete(req.params.id, req.user as User);
			this.handleSuccess(res, null, 'Department deleted', StatusCode.OK);
		} catch (err) {
			this.handleError(res, err);
		}
	}

	/**
	 * Process "get one" request
	 * @param {Express.Request} req - Request
	 * @param {Express.Response} res - Response
	 */
	public async getOne(req: Request, res: Response) {
		try {
			const data = await this.departmentService.findById(
				req.params.id,
				req.user as User,
			);
			this.handleSuccess(res, data, 'Department found', StatusCode.OK);
		} catch (err) {
			this.handleError(res, err);
		}
	}

	/**
	 * Process "get many" request
	 * @param {Express.Request} req - Request
	 * @param {Express.Response} res - Response
	 */
	public async getMany(req: Request, res: Response) {
		try {
			// Get query params
			const queryParams = QueryParams.createFromRequest<DepartmentFindConditions>(
				req.query,
			);
			// Get pagination params from query params
			const paginationParams = PaginationUtil.getParamsFromQueryParams(
				queryParams,
			);

			let data: DepartmentResponse[] | PaginatedData<DepartmentResponse>;
			let dataCount: number;

			// Get paginated data if pagination params were provided
			if (paginationParams) {
				data = await this.departmentService.findManyPaginated(
					req.user as User,
					paginationParams,
					queryParams.params,
				);
				dataCount = data.data.length;
			}
			// Get un-paginated data
			else {
				data = await this.departmentService.findMany(
					req.user as User,
					queryParams.params,
				);
				dataCount = data.length;
			}

			this.handleSuccess(
				res,
				data,
				`${dataCount} departments returned`,
				StatusCode.OK,
			);
		} catch (err) {
			this.handleError(res, err);
		}
	}
}
