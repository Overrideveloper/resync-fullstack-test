import { Service } from 'typedi';
import { Request, Response } from 'express';
import StatusCode from 'http-status-codes';
import { EmployeeService } from '@api/services/employee.service';
import { BaseController } from './base.controller';
import { EmployeeRequest, EmployeeResponse } from '@api/DAL/DTO/employee.dto';
import { User } from '@api/DAL/entity/user.entity';
import { PaginationUtil } from '@api/utilities/pagination.util';
import { QueryParams } from '@api/DAL/models/query-params.model';
import { PaginatedData } from '@api/DAL/models/paginated-data.model';
import { DepartmentFindConditions } from '@api/DAL/models/entity-find-conditions.model';

/**
 * Employee Controller
 * @extends BaseController
 */
@Service()
export class EmployeeController extends BaseController {
	constructor(private employeeService: EmployeeService) {
		super();
	}

	/**
	 * Process create request
	 * @param {Express.Request} req - Request
	 * @param {Express.Response} res - Response
	 */
	public async create(req: Request, res: Response) {
		try {
			const data = await this.employeeService.create(
				EmployeeRequest.createFromRequest(req.body),
				req.user as User,
			);
			this.handleSuccess(res, data, 'Employee created', StatusCode.CREATED);
		} catch (err) {
			this.handleError(res, err);
		}
	}

	/**
	 * Process update request
	 * @param {Express.Request} req - Request
	 * @param {Express.Response} res - Response
	 */
	public async update(req: Request, res: Response) {
		try {
			const data = await this.employeeService.update(
				req.params.id,
				EmployeeRequest.createFromRequest(req.body),
				req.user as User,
			);
			this.handleSuccess(res, data, 'Employee updated', StatusCode.OK);
		} catch (err) {
			this.handleError(res, err);
		}
	}

	/**
	 * Process delete request
	 * @param {Express.Request} req - Request
	 * @param {Express.Response} res - Response
	 */
	public async delete(req: Request, res: Response) {
		try {
			await this.employeeService.delete(req.params.id, req.user as User);
			this.handleSuccess(res, null, 'Employee deleted', StatusCode.OK);
		} catch (err) {
			this.handleError(res, err);
		}
	}

	/**
	 * Process "get one" request
	 * @param {Express.Request} req - Request
	 * @param {Express.Response} res - Response
	 */
	public async getOne(req: Request, res: Response) {
		try {
			const data = await this.employeeService.findById(
				req.params.id,
				req.user as User,
			);
			this.handleSuccess(res, data, 'Employee found', StatusCode.OK);
		} catch (err) {
			this.handleError(res, err);
		}
	}

	/**
	 * Process "get many" request
	 * @param {Express.Request} req - Request
	 * @param {Express.Response} res - Response
	 */
	public async getMany(req: Request, res: Response) {
		try {
			// Get query params
			const queryParams = QueryParams.createFromRequest<DepartmentFindConditions>(
				req.query,
			);
			// Get pagination params from query params
			const paginationParams = PaginationUtil.getParamsFromQueryParams(
				queryParams,
			);

			let data: EmployeeResponse[] | PaginatedData<EmployeeResponse>;
			let dataCount: number;

			// Get paginated data if pagination params were provided
			if (paginationParams) {
				data = await this.employeeService.findManyPaginated(
					req.user as User,
					paginationParams,
					queryParams.params,
				);
				dataCount = data.data.length;
			}
			// Get un-paginated data
			else {
				data = await this.employeeService.findMany(
					req.user as User,
					queryParams.params,
				);
				dataCount = data.length;
			}

			this.handleSuccess(
				res,
				data,
				`${dataCount} employees returned`,
				StatusCode.OK,
			);
		} catch (err) {
			this.handleError(res, err);
		}
	}
}
