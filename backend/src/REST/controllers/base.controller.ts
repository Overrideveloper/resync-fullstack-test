import { HttpError } from '@api/DAL/models/error.model';
import { Response } from 'express';
import { INTERNAL_SERVER_ERROR, OK } from 'http-status-codes';

/**
 * Base Controller
 */
export class BaseController {
	/**
	 * Handle success response
	 * @param {Express.Response} res - Response
	 * @param {any} data - Response Data
	 * @param {string} message - Response message
	 * @param {number} [code] - Response code
	 * @param {boolean} [refreshAuthTokenCookie] - Refresh auth token
	 */
	protected handleSuccess(
		res: Response,
		data: any,
		message: string,
		code: number = OK,
		refreshAuthTokenCookie = true,
	): void {
		res.responz.success(data, message, code, refreshAuthTokenCookie);
	}

	/**
	 * Handle error response
	 * @param {Express.Response} res - Response
	 * @param {Error} data - Error
	 * @param {boolean} [refreshAuthTokenCookie] - Refresh auth token
	 */
	protected handleError(
		res: Response,
		error: HttpError | Error,
		refreshAuthTokenCookie = true,
	): void {
		let errorCode = INTERNAL_SERVER_ERROR;
		if (error instanceof HttpError) {
			errorCode = error.code;
		}

		res.responz.error(null, error.message, errorCode, refreshAuthTokenCookie);
	}
}
