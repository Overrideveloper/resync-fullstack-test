import morgan from 'morgan';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import express, {
	Application,
	NextFunction,
	Request,
	Response,
	Router,
} from 'express';

import responz from '@api/REST/middleware/responz.middleware';
import jwt from 'express-jwt';
import { EnvironmentConfig } from '@api/common/environment.config';
import { JWT_SECRET } from '@api/constants/environment.constants';
import { COOKIE_KEY } from '@api/constants/cookie.constants';

/**
 * REST Server
 */
export class RestServer {
	private server: Application;

	/**
	 * Create new application server
	 * @param {string} routePrefix - URL route prefix
	 * @param {Router[]} routers - Application routers
	 */
	constructor(private routePrefix: string, routers: Router[]) {
		this.server = express();
		this.setupServerMiddleware();
		this.setupServerRouting(routers);
	}

	/**
	 * Server instance
	 */
	public get instance() {
		return this.server;
	}

	/**
	 * Set up server middleware
	 */
	private setupServerMiddleware(): void {
		// Disable x-powered-by header
		this.server.disable('x-powered-by');

		this.server.use(express.json());
		this.server.use(express.urlencoded({ extended: false }));
		this.server.use(morgan(EnvironmentConfig.isProdEnv() ? 'tiny' : 'dev'));
		this.server.use(responz);
		this.server.use(cookieParser());

		// Setup JWT-based Authorization
		this.server.use(
			jwt({
				secret: EnvironmentConfig.getEnvValue(JWT_SECRET),
				algorithms: ['HS256'],
				getToken: (req) => req.cookies[COOKIE_KEY],
			}).unless({
				path: [
					{
						url: `${this.routePrefix}/user/login`,
						method: ['POST'],
					},
					{
						url: `${this.routePrefix}/user/signup`,
						method: ['POST'],
					},
				],
			}),
		);

		// Handle Authorization errors
		this.server.use(
			(err: Error, _: Request, res: Response, __: NextFunction) => {
				if (err.name === 'UnauthorizedError') {
					res.status(401).json({
						code: 401,
						message: 'Unauthorized',
						data: null,
					});
				}
			},
		);

		// Setup CORS
		this.server.use(
			cors({
				origin: true,
				methods: 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
				credentials: true,
				optionsSuccessStatus: 200,
				allowedHeaders: [
					'Access-Control-Allow-Origin',
					'Authorization',
					'Origin',
					'x-requested-with',
					'Content-Type',
					'Content-Range',
					'Content-Disposition',
					'Content-Description',
				],
			}),
		);
	}

	/**
	 * Setup request and response routing
	 * @param {Router[]} routers - Application routers
	 */
	private setupServerRouting(routers: Router[]): void {
		routers.map((router) => this.server.use(this.routePrefix, router));
    this.setupInvalidUrlHandler();
	}

	/**
	 * Handle invalid URL requests
	 */
	private setupInvalidUrlHandler(): void {
		this.server.use((req: Request, res: Response) => {
			res.status(404).json({
				code: 404,
				message: `Route '${req.originalUrl}' does not exist`,
				data: null,
			});
		});
	}
}
