import { WorkDay } from '@api/DAL/entity/department.entity';
import Joi from 'joi';

const validWorkDays = Object.values(WorkDay);

/** Create request data validation schema */
export const create = Joi.object().keys({
	organizationId: Joi.string().required(),
	name: Joi.string().required(),
	description: Joi.string().required(),
	owner: Joi.string().required(),
	workTimes: Joi.object()
		.keys({
			start: Joi.object()
				.keys({
					hour: Joi.number().required(),
					minute: Joi.number().required(),
					second: Joi.number().required(),
				})
				.required(),
			end: Joi.object()
				.keys({
					hour: Joi.number().required(),
					minute: Joi.number().required(),
					second: Joi.number().required(),
				})
				.required(),
		})
		.required(),
	workDays: Joi.array()
		.unique()
		.items(Joi.string().valid(...validWorkDays))
		.min(1)
		.max(validWorkDays.length)
		.required(),
});

/** Update request data validation schema */
export const update = Joi.object().keys({
	name: Joi.string().required(),
	description: Joi.string().required(),
	owner: Joi.string().required(),
	workTimes: Joi.object()
		.keys({
			start: Joi.object()
				.keys({
					hour: Joi.number().required(),
					minute: Joi.number().required(),
					second: Joi.number().required(),
				})
				.required(),
			end: Joi.object()
				.keys({
					hour: Joi.number().required(),
					minute: Joi.number().required(),
					second: Joi.number().required(),
				})
				.required(),
		})
		.required(),
	workDays: Joi.array()
		.unique()
		.items(Joi.string().valid(...validWorkDays))
		.min(1)
		.max(validWorkDays.length)
		.required(),
});
