import Joi from 'joi';

/** Signup request data validation schema */
export const signup = Joi.object().keys({
	email: Joi.string().email().required(),
	password: Joi.string().alphanum().min(7).required(),
	firstName: Joi.string().required(),
	lastName: Joi.string().required(),
});

/** Login request data validation schema */
export const login = Joi.object().keys({
	email: Joi.string().email().required(),
	password: Joi.string().alphanum().min(7).required(),
});
