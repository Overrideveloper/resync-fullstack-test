import Joi from 'joi';

/** Create & update request data validation schema */
export const createAndUpdate = Joi.object().keys({
	name: Joi.string().required(),
	address: Joi.string().required(),
	city: Joi.string().required(),
	state: Joi.string().required(),
	country: Joi.string().required(),
	owner: Joi.string().required(),
});
