import Joi from 'joi';

/** Create and update request data validation schema */
export const createAndUpdate = Joi.object().keys({
	firstName: Joi.string().required(),
	lastName: Joi.string().required(),
	dob: Joi.object()
		.keys({
			year: Joi.number().required(),
			month: Joi.number().required(),
			day: Joi.number().required(),
		})
		.required(),
	workTitle: Joi.string().required(),
	totalExperience: Joi.number().required(),
	departmentId: Joi.string().required(),
});
