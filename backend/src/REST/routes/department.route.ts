import Container from 'typedi';
import { Router, Request, Response } from 'express';

import { ValidatorMiddleware } from '../middleware/validator.middleware';
import { DepartmentController } from '../controllers/department.controller';
import { create, update } from '../validators/department.validator';

// Get DepartmentController from DI Container
const departmentController = Container.get(DepartmentController);
const router = Router();
const route = '/department';

// Department CREATE endpoint
router.post(
	route,
	ValidatorMiddleware.validateBody(create),
	(req: Request, res: Response) => departmentController.create(req, res),
);

// Department UPDATE endpoint
router.patch(
	`${route}/:id`,
	ValidatorMiddleware.validateBody(update),
	(req: Request, res: Response) => departmentController.update(req, res),
);

// Department DELETE endpoint
router.delete(`${route}/:id`, (req: Request, res: Response) =>
	departmentController.delete(req, res),
);

// Department GET (many) endpoint
router.get(route, (req: Request, res: Response) =>
	departmentController.getMany(req, res),
);

// Department GET (one) endpoint
router.get(`${route}/:id`, (req: Request, res: Response) =>
	departmentController.getOne(req, res),
);

export { router as DepartmentRouter };
