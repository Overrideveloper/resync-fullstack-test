import Container from 'typedi';
import { Router, Request, Response } from 'express';

import { ValidatorMiddleware } from '../middleware/validator.middleware';
import { OrganizationController } from '../controllers/organization.controller';
import { createAndUpdate } from '../validators/organization.validator';

// Get OrganizationController from DI Container
const organizationController = Container.get(OrganizationController);
const router = Router();
const route = '/organization';

// Organization CREATE endpoint
router.post(
	route,
	ValidatorMiddleware.validateBody(createAndUpdate),
	(req: Request, res: Response) => organizationController.create(req, res),
);

// Organization UPDATE endpoint
router.put(
	`${route}/:id`,
	ValidatorMiddleware.validateBody(createAndUpdate),
	(req: Request, res: Response) => organizationController.update(req, res),
);

// Organization DELETE endpoint
router.delete(`${route}/:id`, (req: Request, res: Response) =>
	organizationController.delete(req, res),
);

// Organization GET (many) endpoint
router.get(route, (req: Request, res: Response) =>
	organizationController.getMany(req, res),
);

// Organization GET (one) endpoint
router.get(`${route}/:id`, (req: Request, res: Response) =>
	organizationController.getOne(req, res),
);

export { router as OrganizationRouter };
