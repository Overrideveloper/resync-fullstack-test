import Container from 'typedi';
import { Router, Request, Response } from 'express';
import { InsightsController } from '../controllers/insights.controller';

// Get InsightsController from DI Container
const insightsController = Container.get(InsightsController);
const router = Router();

// Entities count insights endpoint
router.get(`/insights/count`, (req: Request, res: Response) =>
	insightsController.getEntitiesCount(req, res),
);

export { router as InsightsRouter };
