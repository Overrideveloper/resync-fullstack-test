import { DepartmentRouter } from './department.route';
import { EmployeeRouter } from './employee.route';
import { InsightsRouter } from './insights.route';
import { OrganizationRouter } from './organization.route';
import { UserRouter } from './user.route';

export default [
	UserRouter,
	OrganizationRouter,
	DepartmentRouter,
	EmployeeRouter,
	InsightsRouter,
];
