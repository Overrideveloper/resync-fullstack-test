import Container from 'typedi';
import { Router, Request, Response } from 'express';

import { ValidatorMiddleware } from '../middleware/validator.middleware';
import { EmployeeController } from '../controllers/employee.controller';
import { createAndUpdate } from '../validators/employee.validator';

// Get EmployeeController from DI Container
const employeeController = Container.get(EmployeeController);
const router = Router();
const route = '/employee';

// Employee CREATE endpoint
router.post(
	route,
	ValidatorMiddleware.validateBody(createAndUpdate),
	(req: Request, res: Response) => employeeController.create(req, res),
);

// Employee UPDATE endpoint
router.put(
	`${route}/:id`,
	ValidatorMiddleware.validateBody(createAndUpdate),
	(req: Request, res: Response) => employeeController.update(req, res),
);

// Employee DELETE endpoint
router.delete(`${route}/:id`, (req: Request, res: Response) =>
	employeeController.delete(req, res),
);

// Employee GET (many) endpoint
router.get(route, (req: Request, res: Response) =>
	employeeController.getMany(req, res),
);

// Employee GET (one) endpoint
router.get(`${route}/:id`, (req: Request, res: Response) =>
	employeeController.getOne(req, res),
);

export { router as EmployeeRouter };
