import Container from 'typedi';
import { Router, Request, Response } from 'express';

import { UserController } from '../controllers/user.controller';
import { signup, login } from '../validators/user.validator';
import { ValidatorMiddleware } from '../middleware/validator.middleware';

// Get UserController from DI Container
const userController = Container.get(UserController);
const router = Router();
const route = '/user';

// User signup endpoint
router.post(
	`${route}/signup`,
	ValidatorMiddleware.validateBody(signup),
	(req: Request, res: Response) => userController.signup(req, res),
);

// User login endpoint
router.post(
	`${route}/login`,
	ValidatorMiddleware.validateBody(login),
	(req: Request, res: Response) => userController.login(req, res),
);

// User "get current user" endpoint
router.get(`${route}/current`, (req: Request, res: Response) =>
	userController.getCurrentUser(req, res),
);

// User logout endpoint
router.get(`${route}/logout`, (req: Request, res: Response) =>
	userController.logoutCurrentUser(req, res),
);

export { router as UserRouter };
