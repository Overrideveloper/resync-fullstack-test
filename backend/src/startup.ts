import Container from 'typedi';
import { fork, on } from 'cluster';
import { cpus } from 'os';
import http from 'http';

import Routers from '@api/REST/routes';
import { RestServer } from '@api/REST/rest-server';
import { EnvironmentConfig } from './common/environment.config';
import { PORT } from './constants/environment.constants';
import { DepartmentController } from './REST/controllers/department.controller';
import { EmployeeController } from './REST/controllers/employee.controller';
import { OrganizationController } from './REST/controllers/organization.controller';
import { DepartmentService } from './services/department.service';
import { EmployeeService } from './services/employee.service';
import { OrganizationService } from './services/organization.service';
import { UserService } from './services/user.service';
import { UserController } from './REST/controllers/user.controller';
import { InsightsService } from './services/insights.service';
import { InsightsController } from './REST/controllers/insights.controller';

export class Startup {
	/**
	 * Configure DI
	 */
	public static configureDependencyInjection() {
		// Import dependencies into DI container
		Container.import([
      DepartmentService,
      EmployeeService,
      InsightsService,
      OrganizationService,
      UserService,
      DepartmentController,
      EmployeeController,
      InsightsController,
      OrganizationController,
      UserController,
    ]);
	}

	/**
	 * Create worker clusters
	 */
	public static spawnCluster(): void {
		const _cpus = cpus().length;

		console.log(`Spawning ${_cpus} Workers`);

		on('listening', ({ id }) => console.log(`Worker ${id} is ready`));

		on('exit', ({ id }) => {
			console.log(`Worker ${id} has exited`);
			fork();
		});

		for (let i = 0; i < _cpus; i++) {
			fork();
		}
	}

	/**
	 * Start up REST HTTP server
	 */
	public static startRestHttpServer(): void {
		const server = new RestServer('/api/v1', Routers);
		const httpServer = http.createServer(server.instance);
		const port = parseInt(EnvironmentConfig.getEnvValue(PORT, 8800));

		httpServer.listen(port);
		httpServer.on('listening', () =>
			console.log(`REST HTTP server Listening on ${port}`),
		);
	}
}
