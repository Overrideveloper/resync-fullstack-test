import bcrypt from 'bcryptjs';

/**
 * Password utilities
 */
export class PasswordUtil {
	/**
	 * Hash password
	 * @param {string} plainTextPassword - Password in plain text
	 * @returns {Promise<string>} Hashed password
	 */
	public static hashPassword(plainTextPassword: string): Promise<string> {
		return new Promise((resolve, reject) => {
			bcrypt.hash(
				plainTextPassword,
				10,
				(err: Error, hashedPassword: string) => {
					if (err) {
						reject(err);
					}

					resolve(hashedPassword);
				},
			);
		});
	}

	/**
	 * Validate password, comparing plain text against hashed value
	 * @param {string} plainTextPassword - Password in plain text
	 * @param {string} hashedPassword - Hash password
	 * @returns {Promise<boolean>} Password validity
	 */
	public static validatePassword(
		plainTextPassword: string,
		hashedPassword: string,
	): Promise<boolean> {
		return new Promise<boolean>((resolve, reject) => {
			bcrypt.compare(plainTextPassword, hashedPassword, (err, result) => {
				if (err) {
					return reject(err);
				}

				resolve(result);
			});
		});
	}
}
