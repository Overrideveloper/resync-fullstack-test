import {
	BadRequestError,
	DuplicateRecordError,
	ForbiddenError,
	InternalServerError,
	InvalidCredentialsError,
	RecordNotFoundError,
} from '@api/DAL/models/error.model';

/**
 * Error utilities
 */
export class ErrorUtil {
	/**
	 * Throw BAD_REQUEST error
	 * @param {string} message - Error message
	 */
	public static throwBadRequestError(message = 'Invalid data provided'): void {
		throw new BadRequestError(message);
	}

	/**
	 * Throw INTERNAL_SERVER_ERROR error
	 * @param {string} message - Error message
	 */
	public static throwInternalServerError(
		message = 'An error occurred. Please try again',
	): void {
		throw new InternalServerError(message);
	}

	/**
	 * Throw RECORD_NOT_FOUND error
	 * @param {string} message - Error message
	 */
	public static throwNotFoundError(message = 'Record not found'): void {
		throw new RecordNotFoundError(message);
	}

	/**
	 * Throw DUPLICATE_RECORD error
	 * @param {string} message - Error message
	 */
	public static throwDuplicateError(message = 'Record already exists'): void {
		throw new DuplicateRecordError(message);
	}

	/**
	 * Throw INVALID_CREDENTIALS error
	 * @param {string} message - Error message
	 */
	public static throwInvalidCredentialsError(
		message = 'Invalid credentials provided',
	): void {
		throw new InvalidCredentialsError(message);
	}

	/**
	 * Throw FORBIDDEN error
	 * @param {string} message - Error message
	 */
	public static throwForbiddenError(
		message = 'You are not authorized to perform this action',
	): void {
		throw new ForbiddenError(message);
	}
}
