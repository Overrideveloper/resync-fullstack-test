import { DEFAULT_NO_OF_RECORDS_PER_PAGE } from '@api/constants/pagination.constants';
import { PaginationParams } from '@api/DAL/models/pagination-params.model';
import { QueryParams } from '@api/DAL/models/query-params.model';

/**
 * Pagination utilities
 */
export class PaginationUtil {
	/**
	 * Get pagination params from QueryParams
	 * @param {QueryParams} query - Query params
	 * @returns {PaginationParams} Pagination params
	 */
	public static getParamsFromQueryParams(
		queryParams: QueryParams<any>,
	): PaginationParams {
		let paginationParams: PaginationParams;

		// Compute pagination params if page and perPage values are valid
		if (
			queryParams?.page &&
			queryParams?.perPage &&
			Number(queryParams?.page) &&
			Number(queryParams?.perPage)
		) {
			const page =
				Number(queryParams.page) && Number(queryParams.page) > 0
					? Number(queryParams.page)
					: 1;
			const perPage =
				Number(queryParams.perPage) || DEFAULT_NO_OF_RECORDS_PER_PAGE;
			const offset = (page - 1) * perPage;

			paginationParams = { page, perPage, offset };
		}

		return paginationParams;
	}
}
