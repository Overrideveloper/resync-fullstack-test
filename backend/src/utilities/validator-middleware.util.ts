import joi, { ValidationError, AnySchema } from 'joi';

const VALIDATION_OPTIONS: joi.ValidationOptions = {
	abortEarly: false,
	stripUnknown: true,
};

/**
 * Validator Middleware utilities
 */
export class ValidatorMiddlewareUtil {
	/**
	 * Parse Validation Error
	 * @param {ValidationError} error - Validation Error
	 * @returns {{ [key: string]: any }} Error map
	 */
	private static parseValidationError(
		error: ValidationError,
	): { [key: string]: any } {
		return error.details.reduce((err, obj) => {
			return { ...err, [obj.context.key]: obj.message.replace(/\"/g, ``) };
		}, {});
	}

	/**
	 * Validate Request Data
	 * @param {any} data - Request data
	 * @param {joi.AnySchema} schema - Validation Schema
	 * @returns {{ err: { [key: string]: any }; value: any }} Valid data and error
	 */
	public static validateReqData(
		data: any,
		schema: AnySchema,
	): { err: { [key: string]: any }; value: any } {
		const { error, value } = joi.validate(data, schema, VALIDATION_OPTIONS);

		if (error) {
			return { err: this.parseValidationError(error), value: null };
		}

		return { err: null, value };
	}
}
