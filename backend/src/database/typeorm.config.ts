import * as fs from 'fs';
import { DatabaseConfig } from './database.config';

/**
 * Generate ormconfig.json file from Database Config
 */
fs.writeFileSync(
	'ormconfig.json',
	JSON.stringify(DatabaseConfig.getTypeOrmConfig(true), null, 2),
);
