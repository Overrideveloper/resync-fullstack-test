import {
	DATABASE_HOST,
	DATABASE_PORT,
	DATABASE_USERNAME,
	DATABASE_PASSWORD,
	DATABASE_NAME,
} from '../constants/environment.constants';
import {
	Connection,
	ConnectionOptions,
	createConnection,
	getConnectionManager,
} from 'typeorm';
import { EnvironmentConfig } from '../common/environment.config';
import { DATABASE_CONNECTION_NAME } from '../constants/database.constants';

/**
 * Database Configuration
 */
export class DatabaseConfig {
	/**
	 * Get TypeOrm connection configuration
	 * @param {boolean} cli - Add CLI configuration
	 * @returns {ConnectionOptions} TypeOrm connection configuration
	 */
	public static getTypeOrmConfig(cli = false): ConnectionOptions {
		let config: ConnectionOptions = {
			type: 'mysql',
			host: EnvironmentConfig.getEnvValue(DATABASE_HOST),
			port: parseInt(EnvironmentConfig.getEnvValue(DATABASE_PORT)),
			username: EnvironmentConfig.getEnvValue(DATABASE_USERNAME),
			password: EnvironmentConfig.getEnvValue(DATABASE_PASSWORD),
			database: EnvironmentConfig.getEnvValue(DATABASE_NAME),
			synchronize: true,
			entities: ['dist/DAL/entity/*.entity.js'],
			ssl: EnvironmentConfig.isProdEnv(),
		};

		// Add CLI configuration if requested
		if (cli === true) {
			config = {
				...config,
				entities: ['src/DAL/entity/*.entity.ts'],
				migrations: ['src/DAL/migration/*.ts'],
				cli: { migrationsDir: 'src/DAL/migration' },
			};
		}

		return config;
	}

	/**
	 * Get database ORM connection
	 * @returns {Connection} Database ORM connection
	 */
	public static async getConnection(): Promise<Connection> {
		let connection: Connection;
		const connectionManager = getConnectionManager();

		// Check if database ORM connection already exists
		if (connectionManager.has(DATABASE_CONNECTION_NAME) === true) {
			// Get existing database connection
			connection = connectionManager.get(DATABASE_CONNECTION_NAME);
			// Re-establish connection if not active
			if (connection.isConnected === false) {
				connection = await connection.connect();
			}
		} else {
			// Create and establish new database connection
			connection = await createConnection(this.getTypeOrmConfig());
		}

		return connection;
	}
}
