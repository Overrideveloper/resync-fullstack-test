import { Column, Entity, ManyToOne } from 'typeorm';
import { BaseEntityModel } from './base.entity';
import { User } from './user.entity';
import { Department } from './department.entity';

export interface DateOfBirth {
  year: number;
  month: number;
  day: number;
}

/**
 * Employee entity model
 * @extends BaseEntityModel
 */
@Entity()
export class Employee extends BaseEntityModel {
	@Column()
	public firstName: string;

	@Column()
	public lastName: string;

	@Column('simple-json')
	public dob: DateOfBirth;

	@Column()
	public workTitle: string;

	@Column()
	public totalExperience: number;

	@ManyToOne(() => Department, { onDelete: 'CASCADE' })
	public department: Department;

	@ManyToOne(() => User, { onDelete: 'CASCADE' })
	public createdBy: User;

	/**
	 * Update model instance with provided data
	 * @param data - Data to update model instance with
	 */
	public updateModel(data: {
		firstName?: string;
		lastName?: string;
		dob?: DateOfBirth;
		workTitle?: string;
		totalExperience?: number;
    department?: Department;
	}): void {
		Object.entries(data).forEach(([key, value]) => {
			this[key] = value;
		});
	}

	/**
	 * Create model instance with provided data
	 * @param data - Data to create model instance with
	 * @returns {Employee} Employee model instance
	 */
	public static createModel(data: {
		firstName: string;
		lastName: string;
		dob: DateOfBirth;
		workTitle: string;
		totalExperience: number;
		department: Department;
		createdBy: User;
	}): Employee {
		const employee = new Employee();

		Object.entries(data).forEach(([key, value]) => {
			employee[key] = value;
		});

		return employee;
	}
}
