import {
	Column,
	CreateDateColumn,
  PrimaryColumn,
	PrimaryGeneratedColumn,
	UpdateDateColumn,
} from 'typeorm';

/**
 * Base entity model
 */
export abstract class BaseEntityModel {
	@PrimaryGeneratedColumn('uuid')
	public id: string;

	@CreateDateColumn()
	public createdAt: Date;

	@UpdateDateColumn()
	public updatedAt: Date;
}
