import { Column, Entity } from 'typeorm';
import { BaseEntityModel } from './base.entity';

/**
 * User entity model
 * @extends BaseEntityModel
 */
@Entity()
export class User extends BaseEntityModel {
	@Column({ unique: true })
	public email: string;

	@Column()
	public password: string;

	@Column()
	public firstName: string;

	@Column()
	public lastName: string;

	/**
	 * Create model instance with provided data
	 * @param data - Data to create model instance with
	 * @returns {User} User model instance
	 */
	public static createModel(data: {
		email: string;
		password: string;
		firstName: string;
		lastName: string;
	}): User {
		const user = new User();
		user.email = data.email;
		user.password = data.password;
		user.firstName = data.firstName;
		user.lastName = data.lastName;

		return user;
	}

	/**
	 * Convert model instance to plain object
	 * @returns {object} User model object
	 */
	public asObject(): object {
		return Object.entries(this).reduce<object>((userObject, [key, value]) => {
			userObject[key] = value;
			return userObject;
		}, {});
	}
}
