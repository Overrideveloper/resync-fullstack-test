import { Column, Entity, ManyToOne } from 'typeorm';
import { BaseEntityModel } from './base.entity';
import { User } from './user.entity';

/**
 * Organization entity model
 * @extends BaseEntityModel
 */
@Entity()
export class Organization extends BaseEntityModel {
	@Column()
	public name: string;

	@Column()
	public address: string;

	@Column()
	public city: string;

	@Column()
	public state: string;

	@Column()
	public country: string;

	@Column()
	public owner: string;

	@ManyToOne(() => User, { onDelete: 'CASCADE' })
	public createdBy: User;

	/**
	 * Update model instance with provided data
	 * @param data - Data to update model instance with
	 */
	public updateModel(data: {
		name?: string;
		address?: string;
		city?: string;
		state?: string;
		country?: string;
		owner?: string;
	}): void {
		Object.entries(data).forEach(([key, value]) => {
			this[key] = value;
		});
	}

	/**
	 * Create model instance with provided data
	 * @param data - Data to create model instance with
	 * @returns {Organization} Organization model instance
	 */
	public static createModel(data: {
		name: string;
		address: string;
		city: string;
		state: string;
		country: string;
		owner: string;
		createdBy: User;
	}): Organization {
		const organization = new Organization();

		Object.entries(data).forEach(([key, value]) => {
			organization[key] = value;
		});

		return organization;
	}
}
