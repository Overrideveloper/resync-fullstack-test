import { Column, Entity, ManyToOne } from 'typeorm';
import { BaseEntityModel } from './base.entity';
import { Organization } from './organization.entity';
import { User } from './user.entity';

// Work time key-value pair key values
type WorkTimeType = 'start' | 'end';

// Work time key-value pair data value
interface WorkTimeData {
  hour: number;
  minute: number;
  second: number;
}

// Work time
export type WorkTime = Record<WorkTimeType, WorkTimeData>;

// List of possible work day values
export enum WorkDay {
	Monday = 'MON',
	Tuesday = 'TUE',
	Wednesday = 'WED',
	Thursday = 'THUR',
	Friday = 'FRI',
	Saturday = 'SAT',
	Sunday = 'SUN',
}

/**
 * Department entity model
 * @extends BaseEntityModel
 */
@Entity()
export class Department extends BaseEntityModel {
	@Column()
	public name: string;

	@Column()
	public description: string;

	@Column()
	public owner: string;

	@Column('simple-json')
	public workTimes: WorkTime;

	@Column('simple-array')
	public workDays: WorkDay[];

	@ManyToOne(() => Organization, { onDelete: 'CASCADE' })
	public organization: Organization;

	@ManyToOne(() => User, { onDelete: 'CASCADE' })
	public createdBy: User;

	/**
	 * Update model instance with provided data
	 * @param data - Data to update model instance with
	 */
	public updateModel(data: {
		name?: string;
		description?: string;
		owner?: string;
		workTimes?: WorkTime;
		workDays?: WorkDay[];
	}): void {
		Object.entries(data).forEach(([key, value]) => {
			this[key] = value;
		});
	}

	/**
	 * Create model instance with provided data
	 * @param data - Data to create model instance with
	 * @returns {Department} Department model instance
	 */
	public static createModel(data: {
		name: string;
		description: string;
		owner: string;
		workTimes: WorkTime;
		workDays: WorkDay[];
		organization: Organization;
		createdBy: User;
	}): Department {
		const department = new Department();

		Object.entries(data).forEach(([key, value]) => {
			department[key] = value;
		});

		return department;
	}
}
