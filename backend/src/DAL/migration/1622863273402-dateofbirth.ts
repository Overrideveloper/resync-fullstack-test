import {MigrationInterface, QueryRunner} from "typeorm";

export class dateofbirth1622863273402 implements MigrationInterface {
    name = 'dateofbirth1622863273402'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `employee` DROP COLUMN `dob`");
        await queryRunner.query("ALTER TABLE `employee` ADD `dob` text NOT NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `employee` DROP COLUMN `dob`");
        await queryRunner.query("ALTER TABLE `employee` ADD `dob` datetime NOT NULL");
    }

}
