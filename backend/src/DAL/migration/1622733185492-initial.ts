import {MigrationInterface, QueryRunner} from "typeorm";

export class initial1622733185492 implements MigrationInterface {
    name = 'initial1622733185492'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("CREATE TABLE `user` (`id` varchar(36) NOT NULL, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), `email` varchar(255) NOT NULL, `password` varchar(255) NOT NULL, `firstName` varchar(255) NOT NULL, `lastName` varchar(255) NOT NULL, UNIQUE INDEX `IDX_e12875dfb3b1d92d7d7c5377e2` (`email`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `organization` (`id` varchar(36) NOT NULL, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), `name` varchar(255) NOT NULL, `address` varchar(255) NOT NULL, `city` varchar(255) NOT NULL, `state` varchar(255) NOT NULL, `country` varchar(255) NOT NULL, `owner` varchar(255) NOT NULL, `createdById` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `department` (`id` varchar(36) NOT NULL, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), `name` varchar(255) NOT NULL, `description` varchar(255) NOT NULL, `owner` varchar(255) NOT NULL, `workTimes` text NOT NULL, `workDays` text NOT NULL, `organizationId` varchar(36) NULL, `createdById` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `employee` (`id` varchar(36) NOT NULL, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), `firstName` varchar(255) NOT NULL, `lastName` varchar(255) NOT NULL, `dob` datetime NOT NULL, `workTitle` varchar(255) NOT NULL, `totalExperience` int NOT NULL, `departmentId` varchar(36) NULL, `createdById` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `organization` ADD CONSTRAINT `FK_acdbd1e490930af04b4ff569ca9` FOREIGN KEY (`createdById`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `department` ADD CONSTRAINT `FK_032f5f4a6d84e0dc9e328e8f114` FOREIGN KEY (`organizationId`) REFERENCES `organization`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `department` ADD CONSTRAINT `FK_66d2edf6d1ebd0caf136b5f63d8` FOREIGN KEY (`createdById`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `employee` ADD CONSTRAINT `FK_9ad20e4029f9458b6eed0b0c454` FOREIGN KEY (`departmentId`) REFERENCES `department`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `employee` ADD CONSTRAINT `FK_7b0acac061179bb6e5c53b4005f` FOREIGN KEY (`createdById`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `employee` DROP FOREIGN KEY `FK_7b0acac061179bb6e5c53b4005f`");
        await queryRunner.query("ALTER TABLE `employee` DROP FOREIGN KEY `FK_9ad20e4029f9458b6eed0b0c454`");
        await queryRunner.query("ALTER TABLE `department` DROP FOREIGN KEY `FK_66d2edf6d1ebd0caf136b5f63d8`");
        await queryRunner.query("ALTER TABLE `department` DROP FOREIGN KEY `FK_032f5f4a6d84e0dc9e328e8f114`");
        await queryRunner.query("ALTER TABLE `organization` DROP FOREIGN KEY `FK_acdbd1e490930af04b4ff569ca9`");
        await queryRunner.query("DROP TABLE `employee`");
        await queryRunner.query("DROP TABLE `department`");
        await queryRunner.query("DROP TABLE `organization`");
        await queryRunner.query("DROP INDEX `IDX_e12875dfb3b1d92d7d7c5377e2` ON `user`");
        await queryRunner.query("DROP TABLE `user`");
    }

}
