import {
	Department,
	WorkDay,
	WorkTime,
} from '../entity/department.entity';
import { OrganizationResponse } from './organization.dto';

/**
 * Department Request DTO
 *
 * A data transfer object for department-related requests
 */
export class DepartmentRequest {
	// Department id
	public id?: string;
	// Organization id
	public organizationId: string;
	// Department name
	public name: string;
	// Department description
	public description: string;
	// Department owner
	public owner: string;
	// Department work times
	public workTimes: WorkTime;
	// Department work days
	public workDays: WorkDay[];

	constructor(data: any = {}) {
		this.organizationId = data?.organizationId ?? '';
		this.name = data?.name ?? '';
		this.description = data?.description ?? '';
		this.owner = data?.owner ?? '';
		this.workTimes = data?.workTimes ?? {};
		this.workDays = data?.workDays ?? [];

		if (data?.id) {
			this.id = data.id;
		}
	}

	/**
	 * Create DTO from HTTP request body
	 * @param {any} request - Request body
	 * @returns {DepartmentRequest} Department request DTO
	 */
	public static createFromRequest(request: any): DepartmentRequest {
		return new DepartmentRequest(request);
	}
}

/**
 * Department Response DTO
 *
 * A data transfer object for Department-related responses
 */
export class DepartmentResponse {
	// Department id
	public id: string;
	// Organization
	public organization: OrganizationResponse;
	// Department name
	public name: string;
	// Department description
	public description: string;
	// Department owner
	public owner: string;
	// Department work times
	public workTimes: WorkTime;
	// Department work days
	public workDays: WorkDay[];

	constructor(data: Department) {
		this.id = data?.id ?? '';
		this.name = data?.name ?? '';
		this.description = data?.description ?? '';
		this.owner = data?.owner ?? '';
		this.workTimes = data?.workTimes ?? ({} as WorkTime);
		this.workDays = data?.workDays ?? [];
		this.organization = data?.organization
			? OrganizationResponse.createFromModel(data.organization)
			: null;
	}

	/**
	 * Create DTO from database model data
	 * @param {Department} model - Database model data
	 * @returns {DepartmentResponse} Department response DTO
	 */
	public static createFromModel(model: Department): DepartmentResponse {
		return new DepartmentResponse(model);
	}
}
