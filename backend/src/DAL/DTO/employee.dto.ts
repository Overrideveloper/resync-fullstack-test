import { DateOfBirth, Employee } from '../entity/employee.entity';
import { DepartmentResponse } from './department.dto';

/**
 * Employee Request DTO
 *
 * A data transfer object for employee-related requests
 */
export class EmployeeRequest {
	// Employee id
	public id?: string;
	// Employee first name
	public firstName: string;
	// Employee last name
	public lastName: string;
	// Employee date of birth
	public dob: DateOfBirth;
	// Employee work title
	public workTitle: string;
	// Employee years of experience
	public totalExperience: number;
	// Employee department id
	public departmentId: string;

	constructor(data: any = {}) {
		this.firstName = data?.firstName ?? '';
		this.lastName = data?.lastName ?? '';
		this.dob = data?.dob ?? {};
		this.workTitle = data?.workTitle ?? '';
		this.totalExperience = data?.totalExperience ?? 0;
		this.departmentId = data?.departmentId ?? '';

		if (data?.id) {
			this.id = data.id;
		}
	}

	/**
	 * Create DTO from HTTP request body
	 * @param {any} request - Request body
	 * @returns {EmployeeRequest} Employee request DTO
	 */
	public static createFromRequest(request: any): EmployeeRequest {
		return new EmployeeRequest(request);
	}
}

/**
 * Employee Response DTO
 *
 * A data transfer object for employee-related responses
 */
export class EmployeeResponse {
	// Employee id
	public id?: string;
	// Employee first name
	public firstName: string;
	// Employee last name
	public lastName: string;
	// Employee date of birth
	public dob: DateOfBirth;
	// Employee work title
	public workTitle: string;
	// Employee years of experience
	public totalExperience: number;
	// Employee department
	public department: DepartmentResponse;

	constructor(data: Employee) {
		this.id = data?.id ?? '';
		this.firstName = data?.firstName ?? '';
		this.lastName = data?.lastName ?? '';
		this.dob = data?.dob ?? ({} as DateOfBirth);
		this.workTitle = data?.workTitle ?? '';
		this.totalExperience = data?.totalExperience ?? 0;
		this.department = data?.department
			? DepartmentResponse.createFromModel(data.department)
			: null;
	}

	/**
	 * Create DTO from database model data
	 * @param {Employee} model - Database model data
	 * @returns {EmployeeResponse} Employee response DTO
	 */
	public static createFromModel(model: Employee): EmployeeResponse {
		return new EmployeeResponse(model);
	}
}
