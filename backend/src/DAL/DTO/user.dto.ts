import { User } from '../entity/user.entity';

/**
 * Signup Request DTO
 *
 * A data transfer object for signup request
 */
export class SignupRequest {
	// Email address
	public email: string;
	// Password
	public password: string;
	// First name
	public firstName: string;
	// Last name
	public lastName: string;

	constructor(data: any = {}) {
		this.email = data?.email ?? '';
		this.password = data?.password ?? '';
		this.firstName = data?.firstName ?? '';
		this.lastName = data?.lastName ?? '';
	}

	/**
	 * Create DTO from HTTP request body
	 * @param {any} request - Request body
	 * @returns {SignupRequest} Signup request DTO
	 */
	public static createFromRequest(request: any): SignupRequest {
		return new SignupRequest(request);
	}
}

/**
 * Login Request DTO
 *
 * A data transfer object for login request
 */
export class LoginRequest {
	// Email address
	public email: string;
	// Password
	public password: string;

	constructor(data: any = {}) {
		this.email = data?.email ?? '';
		this.password = data?.password ?? '';
	}

	/**
	 * Create DTO from HTTP request body
	 * @param {any} request - Request body
	 * @returns {LoginRequest} Login request DTO
	 */
	public static createFromRequest(request: any): LoginRequest {
		return new LoginRequest(request);
	}
}

/**
 * User Response DTO
 *
 * A data transfer object for user data.
 *
 * User in signup and login response
 */
export class UserResponse {
	// User id
	public id: string;
	// Email address
	public email: string;
	// First name
	public firstName: string;
	// Last name
	public lastName: string;

	constructor(data: {
		id: string;
		email: string;
		firstName: string;
		lastName: string;
	}) {
		this.id = data?.id ?? '';
		this.email = data?.email ?? '';
		this.firstName = data?.firstName ?? '';
		this.lastName = data?.lastName ?? '';
	}

	/**
	 * Create DTO from database model data
	 * @param {User} model - Database model data
	 * @returns {UserResponse} User response DTO
	 */
	public static createFromModel(model: User): UserResponse {
		return new UserResponse(model);
	}
}
