import { Organization } from '../entity/organization.entity';

/**
 * Organization Request DTO
 *
 * A data transfer object for organization-related requests
 */
export class OrganizationRequest {
	// Organization id
	public id?: string;
	// Organization name
	public name: string;
	// Organization address
	public address: string;
	// Organization city
	public city: string;
	// Organization state
	public state: string;
	// Organization country
	public country: string;
	// Organization owner
	public owner: string;

	constructor(data: any = {}) {
		this.name = data?.name ?? '';
		this.address = data?.address ?? '';
		this.city = data?.city ?? '';
		this.state = data?.state ?? '';
		this.country = data?.country ?? '';
		this.owner = data?.owner ?? '';

		if (data?.id) {
			this.id = data.id;
		}
	}

	/**
	 * Create DTO from HTTP request body
	 * @param {any} request - Request body
	 * @returns {OrganizationRequest} Organization request DTO
	 */
	public static createFromRequest(request: any): OrganizationRequest {
		return new OrganizationRequest(request);
	}
}

/**
 * Organization Response DTO
 *
 * A data transfer object for organization-related responses
 */
export class OrganizationResponse {
	// Organization id
	public id?: string;
	// Organization name
	public name: string;
	// Organization address
	public address: string;
	// Organization city
	public city: string;
	// Organization state
	public state: string;
	// Organization country
	public country: string;
	// Organization owner
	public owner: string;

	constructor(data: Organization) {
		this.id = data?.id ?? '';
		this.name = data?.name ?? '';
		this.address = data?.address ?? '';
		this.city = data?.city ?? '';
		this.state = data?.state ?? '';
		this.country = data?.country ?? '';
		this.owner = data?.owner ?? '';
	}

	/**
	 * Create DTO from database model data
	 * @param {Organization} model - Database model data
	 * @returns {OrganizationResponse} Organization response DTO
	 */
	public static createFromModel(model: Organization): OrganizationResponse {
		return new OrganizationResponse(model);
	}
}
