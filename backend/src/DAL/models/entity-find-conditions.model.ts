import { OrganizationRequest } from '@api/DAL/DTO/organization.dto';
import { FindConditions } from 'typeorm';
import { DepartmentRequest } from '../DTO/department.dto';
import { EmployeeRequest } from '../DTO/employee.dto';

export interface OrganizationFindConditions
	extends FindConditions<OrganizationRequest> {
	createdById?: string;
}

export interface DepartmentFindConditions
	extends FindConditions<DepartmentRequest> {
	createdById?: string;
}

export interface EmployeeFindConditions
	extends FindConditions<EmployeeRequest> {
	createdById?: string;
}
