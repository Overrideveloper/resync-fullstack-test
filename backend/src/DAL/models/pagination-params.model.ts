/**
 * Pagination Params
 */
export interface PaginationParams {
	// Page to return
	page: number;
	// No of records per page
	perPage: number;
	// No of docs to offset
	offset: number;
}
