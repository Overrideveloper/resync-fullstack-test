import {
	BAD_REQUEST,
	FORBIDDEN,
	INTERNAL_SERVER_ERROR,
	NOT_FOUND,
	UNAUTHORIZED,
	UNPROCESSABLE_ENTITY,
} from 'http-status-codes';

/**
 * Repository Error Types
 */
export enum HttpErrorType {
	// Duplicate Record Error
	DuplicateRecord = 'DUPLICATE_RECORD',
	// Record Not Found Error
	RecordNotFound = 'RECORD_NOT_FOUND',
	// Invalid Credentials Error
	InvalidCredentials = 'INVALID_CREDENTIALS',
	// Bad Request Error
	BadRequest = 'BAD_REQUEST',
	// Internal Server Error
	InternalServer = 'INTERNAL_SERVER_ERROR',
	// Unauthorized Error
	Unauthorized = 'UNAUTHORIZED',
	// Forbidden Error
	Forbidden = 'FORBIDDEN',
}

/**
 * Base HttpError
 * @extends Error
 */
export class HttpError {
	constructor(
		public message: string,
		public name: HttpErrorType,
		public code: number,
	) {}
}

/**
 * Record Not Found Error
 * @extends HttpError
 */
export class RecordNotFoundError extends HttpError {
	constructor(message: string) {
		super(message, HttpErrorType.RecordNotFound, NOT_FOUND);
	}
}

/**
 * Duplicate Record Error
 * @extends HttpError
 */
export class DuplicateRecordError extends HttpError {
	constructor(message: string) {
		super(message, HttpErrorType.DuplicateRecord, BAD_REQUEST);
	}
}

/**
 * Bad Request Error
 * @extends HttpError
 */
export class BadRequestError extends HttpError {
	constructor(message: string) {
		super(message, HttpErrorType.BadRequest, BAD_REQUEST);
	}
}

/**
 * Invalid Credentials Error
 * @extends HttpError
 */
export class InvalidCredentialsError extends HttpError {
	constructor(message: string) {
		super(message, HttpErrorType.InvalidCredentials, UNPROCESSABLE_ENTITY);
	}
}

/**
 * Internal Server Error
 * @extends HttpError
 */
export class InternalServerError extends HttpError {
	constructor(message: string) {
		super(message, HttpErrorType.InternalServer, INTERNAL_SERVER_ERROR);
	}
}

/**
 * Unauthorized Error
 * @extends HttpError
 */
export class UnauthorizedError extends HttpError {
	constructor(message: string) {
		super(message, HttpErrorType.Unauthorized, UNAUTHORIZED);
	}
}

/**
 * Forbidden Error
 * @extends HttpError
 */
export class ForbiddenError extends HttpError {
	constructor(message: string) {
		super(message, HttpErrorType.Forbidden, FORBIDDEN);
	}
}
