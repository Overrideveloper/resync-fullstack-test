import { DEFAULT_NO_OF_RECORDS_PER_PAGE } from '@api/constants/pagination.constants';

/**
 * Model for paginated data
 */
export class PaginatedData<T> {
	// Current page
	public page: number;
	// No of records returned per page
	public pageSize: number;
	// Total number of pages for the requested resource
	public pageCount: number;
	// Total number of records for the requested resource
	public totalDataCount: number;
	// Records returned for requested resource
	public data: T[];

	constructor(data: {
		page: any;
		pageSize: any;
		pageCount: number;
		totalDataCount: number;
		data: T[];
	}) {
		this.page = Number(data?.page) ?? 1;
		this.pageSize = Number(data?.pageSize) ?? DEFAULT_NO_OF_RECORDS_PER_PAGE;
		this.pageCount = data?.pageCount ?? 0;
		this.totalDataCount = data?.totalDataCount ?? 0;
		this.data = data?.data ?? [];
	}

	/**
	 * Create model from returned records
	 * @param data
	 * @returns {PaginatedData} Paginated data
	 */
	public static createFromPaginatedRecords<T>(data: {
		page: number;
		pageSize: number;
		pageCount: number;
		totalDataCount: number;
		data: T[];
	}): PaginatedData<T> {
		return new PaginatedData<any>(data);
	}
}
