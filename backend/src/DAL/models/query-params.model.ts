/**
 * Model for HTTP query params
 */
export class QueryParams<T> {
	// Query params for filtering records
	public params: T;
	// [Pagination] Page to return
	public page?: number;
	// [Pagination] Number of records per page
	public perPage?: number;

	constructor(data: { params?: T; page?: number; perPage?: number }) {
		this.params = data?.params ?? null;

		if (data?.page) {
			this.page = data.page;
		}

		if (data?.perPage) {
			this.perPage = data.perPage;
		}
	}

	/**
	 * Create model from HTTP request params
	 * @param {any} request - Request params
	 * @returns {Query} QueryParams model
	 */
	public static createFromRequest<T>(request: any = {}): QueryParams<T> {
		let data = {};
		const paramKeys = Object.keys(request);
		const params = { ...request };

		// Isolate page & perPage query params from other params
		// The other params will be used as the QueryParams.params value
		['page', 'perPage'].forEach((key) => {
			if (paramKeys.includes(key)) {
				data[key] = params[key];
				delete params[key];
			}
		});

		// Delete null/undefined values
		Object.keys(params).forEach((key) => {
			if (params[key] === null || params[key] === undefined) {
				delete params[key];
			}
		});

		return new QueryParams({
			...(data as T),
			params,
		});
	}
}
