import { CookieOptions } from 'express';

// Response cookie options
export const COOKIE_OPTIONS: CookieOptions = {
	maxAge: 3600000,
	httpOnly: true,
};

// Response cookie key
export const COOKIE_KEY = 'RESYNC_AUTH';
