import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InterstitialComponent } from './interstitial.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: InterstitialComponent,
  },
];

@NgModule({
  declarations: [InterstitialComponent],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InterstitialModule {}
