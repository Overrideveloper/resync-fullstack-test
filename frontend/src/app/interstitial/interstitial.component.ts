import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UserService } from '../shared/service/user.service';
import {
  AuthorizationState,
  AuthorizationStore,
} from '../shared/store/authorization.store';

@Component({
  selector: 'app-interstitial',
  templateUrl: './interstitial.component.html',
  styles: [':host { height: 100%; width: 100%; }'],
})
export class InterstitialComponent implements OnInit, OnDestroy {
  private redirectUrl!: string;
  private queryParamMapSubscription!: Subscription;

  constructor(
    private readonly userService: UserService,
    private readonly authorizationStore: AuthorizationStore,
    private readonly router: Router,
    readonly route: ActivatedRoute
  ) {
    this.queryParamMapSubscription = route.queryParamMap.subscribe(
      (queryParamMap) => {
        this.redirectUrl = (queryParamMap.get(
          'redirectTo'
        ) as unknown) as string;
      }
    );
  }

  public ngOnInit(): void {
    // Get currently authenticated user from backend using the authentication HttpOnly cookie
    this.userService.getCurrentUser().subscribe(
      (user) => {
        // Currently authenticated user found, set authorization state and user data in store
        this.authorizationStore.setAuthorizationState(
          AuthorizationState.Authorized
        );
        this.authorizationStore.setUser(user);

        // Navigate to redirectUrl or dashboard
        if (this.redirectUrl) {
          this.router.navigateByUrl(this.redirectUrl);
        } else {
          this.router.navigate(['dashboard']);
        }
      },
      (error) => {
        // No currently authenticated user found, redirect to login page
        if (error instanceof HttpErrorResponse && error.status === 404) {
          this.router.navigate(['auth', 'login']);
        }
      }
    );
  }

  public ngOnDestroy(): void {
    this.queryParamMapSubscription.unsubscribe();
  }
}
