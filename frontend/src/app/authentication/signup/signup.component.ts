import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { Login } from 'src/app/shared/model/login.model';
import { Signup } from 'src/app/shared/model/signup.model';
import { UserService } from 'src/app/shared/service/user.service';
import {
  AuthorizationState,
  AuthorizationStore,
} from 'src/app/shared/store/authorization.store';
import { CustomFormValidator } from 'src/app/shared/utility/validator.util';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styles: [':host { height: 100%; width: 100%; }'],
})
export class SignupComponent {
  public signupForm!: FormGroup;
  public emailAlreadyInUse: boolean = false;
  public submitted: boolean = false;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly userService: UserService,
    private readonly authorizationStore: AuthorizationStore,
    private readonly router: Router
  ) {
    this.buildSignupForm();
  }

  /**
   * Get signup form control objects
   */
  public get signupFormControls(): Record<string, AbstractControl> {
    return this.signupForm.controls;
  }

  /**
   * Submit signup form and make request to backend
   */
  public submitSignupForm(): void {
    // Only proceed if form is valid
    if (this.signupForm.valid) {
      // Set submitted state to true. This will trigger the form loading UI state
      this.submitted = true;
      // Hide "email already in use" error in UI, if displayed
      this.emailAlreadyInUse = false;

      // Create signup data from form data
      const signupData = Signup.createFromForm(this.signupForm.value);

      // Make signup request
      this.userService.signup(signupData).subscribe(
        (_) => {
          // Create login data from form data
          const loginData = Login.createFromForm(this.signupForm.value);

          // Make login request
          this.userService.login(loginData).subscribe(
            (user) => {
              // Set authorization state and user data in store
              this.authorizationStore.setAuthorizationState(
                AuthorizationState.Authorized
              );
              this.authorizationStore.setUser(user);

              // Navigate to dashboard
              this.router.navigate(['dashboard']);
            },
            () => {
              // Set submitted state to false. This will trigger the form default UI state
              this.submitted = false;
            },
          );
        },
        (error) => {
          // "Email already in use" error
          if (error instanceof HttpErrorResponse && error.status === 422) {
            this.emailAlreadyInUse = true;
          }

          // Set submitted state to false. This will trigger the form default UI state
          this.submitted = false;
        }
      );
    }
  }

  /**
   * Build signup form
   */
  private buildSignupForm(): void {
    this.signupForm = this.formBuilder.group(
      {
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        email: [
          '',
          Validators.compose([Validators.required, Validators.email]),
        ],
        password: [
          '',
          Validators.compose([Validators.required, Validators.minLength(7)]),
        ],
        confirmPassword: [
          '',
          Validators.compose([Validators.required, Validators.minLength(7)]),
        ],
      },
      {
        validators: [CustomFormValidator.confirmPassword],
      }
    );
  }
}
