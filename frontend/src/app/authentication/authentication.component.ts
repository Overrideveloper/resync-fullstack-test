import { Component } from '@angular/core';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styles: [':host { height: 100%; width: 100%; }'],
})
export class AuthenticationComponent {}
