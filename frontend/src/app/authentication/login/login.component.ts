import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { Login } from 'src/app/shared/model/login.model';
import { UserService } from 'src/app/shared/service/user.service';
import {
  AuthorizationState,
  AuthorizationStore,
} from 'src/app/shared/store/authorization.store';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: [':host { height: 100%; width: 100%; }'],
})
export class LoginComponent {
  public loginForm!: FormGroup;
  public invalidCredentials: boolean = false;
  public submitted: boolean = false;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly userService: UserService,
    private readonly authorizationStore: AuthorizationStore,
    private readonly router: Router
  ) {
    this.buildLoginForm();
  }

  /**
   * Get login form control objects
   */
  public get loginFormControls(): Record<string, AbstractControl> {
    return this.loginForm.controls;
  }

  /**
   * Submit login form and make request to backend
   */
  public submitLoginForm(): void {
    // Only proceed if form is valid
    if (this.loginForm.valid) {
      // Set submitted state to true. This will trigger the form loading UI state
      this.submitted = true;
      // Hide invalid credentials error in UI, if displayed
      this.invalidCredentials = false;

      // Create login data from form data
      const loginData = Login.createFromForm(this.loginForm.value);

      // Make login request
      this.userService.login(loginData).subscribe(
        (user) => {
          // Set authorization state and user data in store
          this.authorizationStore.setAuthorizationState(
            AuthorizationState.Authorized
          );
          this.authorizationStore.setUser(user);

          // Navigate to dashboard
          this.router.navigate(['dashboard']);
        },
        (error) => {
          // Invalid credentials error
          if (error instanceof HttpErrorResponse && error.status === 422) {
            this.invalidCredentials = true;
          }
          // Set submitted state to false. This will trigger the form default UI state
          this.submitted = false;
        },
      );
    }
  }

  /**
   * Build login form
   */
  private buildLoginForm(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: [
        '',
        Validators.compose([Validators.required, Validators.minLength(7)]),
      ],
    });
  }
}
