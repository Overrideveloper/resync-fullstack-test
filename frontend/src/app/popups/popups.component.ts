import {
  Component,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { ActionPopup } from '../shared/model/popup.interface';
import { PopupsService } from '../shared/service/popups.service';
import { PopupsAction } from '../shared/model/popups-action.enum';

@Component({
  selector: 'app-popups',
  templateUrl: './popups.component.html',
  styleUrls: ['./popups.component.scss'],
})
export class PopupsComponent implements OnInit, OnDestroy {
  public PopupsAction = PopupsAction;
  // Data for currently open action popup
  public actionPopupData!: ActionPopup;

  // Track subscriptions to data streams
  private subscriptionList: Subscription[] = [];

  // Popup template references
  @ViewChild('actionPopup') private actionPopup!: TemplateRef<any>;

  constructor(
    private readonly popupsService: PopupsService,
    private readonly modalService: NgbModal
  ) {}

  public ngOnInit(): void {
    this.listenToPopupEvents();
  }

  public ngOnDestroy(): void {
    // Unsubscribe from data streams when destroying component
    this.subscriptionList.forEach((subscription) => subscription.unsubscribe());
  }

  /**
   * Listen to events from the PopupsService and open popups when requested
   */
  private listenToPopupEvents(): void {
    this.subscriptionList.push(
      // Open action popup when requested
      this.popupsService.displayActionPopup$.subscribe((popupData) => {
        this.actionPopupData = popupData;

        this.modalService
          .open(
            this.actionPopup,
            {
              ariaLabelledBy: 'model-basic-title',
              backdrop: 'static',
            }
          )
          .result.then(
            (action) => {
              // Handle dispatched popup action
              switch (action) {
                case popupData.action:
                  // Invoke action callback
                  popupData.callback();
                  break;

                // Throw error for unimplemented cases
                default:
                  throw new Error(`Case ${action} unimplemented`);
              }
            },
            () => {
              return;
            }
          );
      })
    );
  }
}
