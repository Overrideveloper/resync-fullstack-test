import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PopupsComponent } from './popups.component';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [PopupsComponent],
  imports: [CommonModule, NgbModalModule],
  exports: [PopupsComponent],
})
export class PopupsModule {}
