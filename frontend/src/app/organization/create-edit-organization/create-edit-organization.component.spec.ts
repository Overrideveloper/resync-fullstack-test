import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEditOrganizationComponent } from './create-edit-organization.component';

describe('CreateEditOrganizationComponent', () => {
  let component: CreateEditOrganizationComponent;
  let fixture: ComponentFixture<CreateEditOrganizationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateEditOrganizationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEditOrganizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
