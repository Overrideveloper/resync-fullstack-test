import { LocationStrategy } from '@angular/common';
import { Component, OnDestroy } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Organization } from 'src/app/shared/model/entity/organization.model';
import { OrganizationService } from 'src/app/shared/service/entity/organization.service';
import { ToastService } from 'src/app/shared/service/toast.service';

@Component({
  selector: 'app-create-edit-organization',
  templateUrl: './create-edit-organization.component.html',
  styleUrls: [],
})
export class CreateEditOrganizationComponent implements OnDestroy {
  public mode!: 'create' | 'edit';
  public organization!: Organization;
  public submitted = false;
  public createEditForm!: FormGroup;

  private activatedRouteSubscription!: Subscription;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly organizationService: OrganizationService,
    private readonly router: Router,
    private readonly locationStrategy: LocationStrategy,
    private readonly activatedRoute: ActivatedRoute,
    private readonly toastService: ToastService,
  ) {
    this.listenToRouteParams();
  }

  public ngOnDestroy(): void {
    this.activatedRouteSubscription?.unsubscribe();
  }

  /**
   * Get form control objects
   */
  public get controls(): Record<string, AbstractControl> {
    return this.createEditForm.controls;
  }

  /**
   * Setup form with provided organization (edit mode) or empty values (create mode)
   */
  public setupForm(organization?: Organization): void {
    this.createEditForm = this.formBuilder.group({
      name: [organization?.name ?? '', Validators.required],
      address: [organization?.address ?? '', Validators.required],
      city: [organization?.city ?? '', Validators.required],
      state: [organization?.state ?? '', Validators.required],
      country: [organization?.country ?? '', Validators.required],
      owner: [organization?.owner ?? '', Validators.required],
    });
  }

  /**
   * Submit form data and make request to backend
   */
  public submitForm() {
    if (this.createEditForm.valid === true) {
      // Trigger form submitting UI state
      this.submitted = true;

      switch (this.mode) {
        case 'create':
          this.submitCreateForm();
          break;

        case 'edit':
          this.submitEditForm();
          break;

        default:
          throw new Error(`Case "${this.mode}" not yet implemented`);
      }
    }
  }

  /**
   * Submit form for create mode
   */
  private submitCreateForm(): void {
    const organizationToCreate = Organization.createFromForm(this.createEditForm.value);

    this.organizationService.create(organizationToCreate).subscribe(
      (organization) => {
        // Display success toast and navigate to list page
        this.toastService.show(`Organization "${organization.name}" created.`, {
          className: 'bg-primary text-light opacity-4',
          delay: 3000,
        });
        this.router.navigate(['organizations']);
      },
      // Trigger form default UI state
      (_) => this.submitted = false,
    );
  }

  /**
   * Submit form for edit mode
   */
  private submitEditForm(): void {
    const organizationToEdit = this.organization.clone();
    organizationToEdit.updateFromForm(this.createEditForm.value);

    this.organizationService.update(organizationToEdit).subscribe(
      (organization) => {
        // Display success toast and navigate to list page
        this.toastService.show(`Organization "${organization.name}" updated.`, {
          className: 'bg-primary text-light opacity-4',
          delay: 3000,
        });
        this.router.navigate(['organizations']);
      },
      // Trigger form default UI state
      (_) => this.submitted = false,
    );
  }

  /**
   * Listen to route params to determine waht form mode to setup, create or edit
   */
  private listenToRouteParams(): void {
    const routePath = this.locationStrategy.path();

    // Route is "new", setup create form
    if (routePath.includes('new')) {
      this.mode = 'create';
      this.setupForm();
    }

    // Route is "edit", load organization and setup edit form
    if (routePath.includes('edit')) {
      this.activatedRouteSubscription = this.activatedRoute.paramMap.subscribe((params) => {
        const organizationId = params.get('organizationId');

        if (organizationId) {
          this.mode = 'edit';
          this.organizationService.get(organizationId).subscribe((organization) => {
            this.organization = organization;
            this.setupForm(organization);
          });
        }
      });
    }
  }
}
