import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateEditOrganizationComponent } from './create-edit-organization/create-edit-organization.component';
import { OrganizationListComponent } from './organization-list/organization-list.component';
import { OrganizationComponent } from './organization.component';

const routes: Routes = [
  {
    path: '',
    component: OrganizationComponent,
    children: [
      {
        path: '',
        component: OrganizationListComponent,
      },
      {
        path: 'new',
        component: CreateEditOrganizationComponent,
      },
      {
        path: 'edit/:organizationId',
        component: CreateEditOrganizationComponent,
      },
      {
        path: '**',
        redirectTo: '',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrganizationRoutingModule { }
