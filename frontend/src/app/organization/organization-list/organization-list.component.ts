import { Component, OnInit } from '@angular/core';
import { PaginationInstance } from 'ngx-pagination';
import { BehaviorSubject, Observable } from 'rxjs';
import { Organization } from 'src/app/shared/model/entity/organization.model';
import { PopupsAction } from 'src/app/shared/model/popups-action.enum';
import { RequestOptions } from 'src/app/shared/model/request-options.model';
import { ResponsePagination } from 'src/app/shared/model/response-pagination.model';
import { OrganizationService } from 'src/app/shared/service/entity/organization.service';
import { PopupsService } from 'src/app/shared/service/popups.service';
import { ToastService } from 'src/app/shared/service/toast.service';

interface OrganizationListResponse {
  data: Organization[];
  pagination: ResponsePagination;
}

@Component({
  selector: 'app-organization-list',
  templateUrl: './organization-list.component.html',
  styleUrls: [],
})
export class OrganizationListComponent implements OnInit {
  public organizationList!: Organization[];
  public pagination!: PaginationInstance;
  public page = 1;

  // Loading state control
  private loadingSubject = new BehaviorSubject<boolean>(true);
  public readonly loading$: Observable<boolean> = this.loadingSubject.asObservable();

  constructor(
    private readonly organizationService: OrganizationService,
    private readonly toastService: ToastService,
    private readonly popupsService: PopupsService
  ) {}

  public ngOnInit(): void {
    this.loadOrganizationList();
  }

  /**
   * Handle pagination page change
   */
  public handlePageChange(page: number): void {
    this.page = page;
    this.loadOrganizationList();
  }

  /**
   * Open delete confirmation popup
   */
  public openDeleteConfirmationPopup(organization: Organization) {
    this.popupsService.showActionPopup({
      action: PopupsAction.Delete,
      actionText: 'Delete',
      actionTextClassName: 'btn btn-danger',
      callback: this.deleteOrganization.bind(this, organization),
      title: `Delete organization "${organization.name}"`,
      description: 'Are you sure you want to delete this organization?',
    });
  }

  /**
   * Delete selected organization
   */
  private deleteOrganization(organization: Organization) {
    this.organizationService.delete(organization.id as string).subscribe(() => {
      // Show success toast
      this.toastService.show(`Organization "${organization.name}" deleted`, {
        className: 'bg-primary text-light opacity-4',
        delay: 3000,
      });
      // Reload list
      this.loadOrganizationList();
    });
  }

  /**
   * Load organization list
   */
  private loadOrganizationList(): void {
    const requestOptions = RequestOptions.create({
      page: this.page,
      perPage: 8,
    });

    this.loadingSubject.next(true);

    this.organizationService.getList(requestOptions).subscribe((result) => {
      const { pagination, data } = result as OrganizationListResponse;
      this.pagination = {
        itemsPerPage: pagination.pageSize,
        currentPage: pagination.page,
        totalItems: pagination.totalDataCount,
      };

      this.organizationList = data;
      this.loadingSubject.next(false);
    });
  }
}
