import { Component, OnInit } from '@angular/core';
import { CalendarView } from 'angular-calendar';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: []
})
export class CalendarComponent implements OnInit {
  public view = CalendarView.Month;
  public CalendarView = CalendarView;
  public viewDate = new Date();
  public activeDayIsOpen = true;

  constructor() { }

  ngOnInit(): void {
  }

  public closeOpenMonthViewDay(): void {
    this.activeDayIsOpen = false;
  }

  public setView(view: CalendarView) {
    this.view = view;
  }
}
