import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { PopupsAction } from '../shared/model/popups-action.enum';
import { User } from '../shared/model/user.model';
import { PopupsService } from '../shared/service/popups.service';
import { UserService } from '../shared/service/user.service';
import {
  AuthorizationState,
  AuthorizationStore,
} from '../shared/store/authorization.store';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: [],
})
export class NavComponent {
  public navbarToggled = false;

  constructor(
    private readonly userService: UserService,
    private readonly authorizationStore: AuthorizationStore,
    private readonly router: Router,
    private readonly popupsService: PopupsService
  ) {}

  /**
   * Show logout confirmation popup
   */
  public showLogoutConfirmationPopup() {
    this.popupsService.showActionPopup({
      action: PopupsAction.Logout,
      actionText: 'Log out',
      actionTextClassName: 'btn btn-danger',
      callback: this.logout.bind(this),
      description: 'Are you sure you want to log out?',
      title: 'Log out',
    });
  }

  /**
   * Log current user out
   */
  private logout(): void {
    this.userService.logoutCurrentUser().subscribe(() => {
      // Set authorization state and user in store
      this.authorizationStore.setAuthorizationState(
        AuthorizationState.Unauthorized
      );
      this.authorizationStore.setUser((null as unknown) as User);
      // Navigate to login page
      this.router.navigate(['auth', 'login']);
    });
  }
}
