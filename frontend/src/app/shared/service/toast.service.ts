import { Injectable } from '@angular/core';
import { Toast, ToastConfig } from '../model/toast.interface';

@Injectable({
  providedIn: 'root',
})
export class ToastService {
  public toastList: Toast[] = [];

  /**
   * Show toast with given text and options
   */
  public show(text: string, options = {} as ToastConfig): Promise<Toast> {
    const toast = { text, ...(options as any) };
    this.toastList.push(toast);

    // Return a promise when the toast has been displayed
    return new Promise((resolve) => {
      // Calculate delay before resolving the promise;
      const timeout = options?.delay || 500;
      setTimeout(() => resolve(toast), timeout);
    });
  }

  /**
   * Hide toast
   */
  public hide(toastToHide: Toast): void {
    this.toastList = this.toastList.filter((toast) => toast !== toastToHide);
  }
}
