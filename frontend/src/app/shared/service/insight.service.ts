import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { InsightCount } from '../model/insight-count.model';

@Injectable({
  providedIn: 'root',
})
export class InsightService {
  private readonly url = `${environment.apiUrl}/insights`;
  constructor(private readonly http: HttpClient) {}

  /**
   * Get insights for entities count
   */
  public getEntitiesCount(): Observable<InsightCount> {
    return this.http
      .get(`${this.url}/count`)
      .pipe(map((response: any) => InsightCount.createFromAPI(response)));
  }
}
