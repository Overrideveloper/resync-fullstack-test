import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Employee } from '../../model/entity/employee.model';
import { EntityService } from './entity.service';

@Injectable({
  providedIn: 'root',
})
export class EmployeeService extends EntityService<Employee> {
  constructor(readonly httpClient: HttpClient) {
    super(httpClient, '/employee', Employee.createFromAPI);
  }
}
