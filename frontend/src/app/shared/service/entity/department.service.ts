import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Department } from '../../model/entity/department.model';
import { EntityService } from './entity.service';

@Injectable({
  providedIn: 'root',
})
export class DepartmentService extends EntityService<Department> {
  constructor(private readonly httpClient: HttpClient) {
    super(httpClient, '/department', Department.createFromAPI);
  }

  /**
   * Update department
   */
  public update(department: Department): Observable<Department> {
    const payload = department.processForAPI();
    // Delete organizationId as organization cannot be updated for a department
    delete payload.organizationId;
    return this.httpClient
      .patch(`${this.url}/${department.id}`, payload)
      .pipe(map((response: any) => Department.createFromAPI(response)));
  }
}
