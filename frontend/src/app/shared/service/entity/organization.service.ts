import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Organization } from '../../model/entity/organization.model';
import { EntityService } from './entity.service';

@Injectable({
  providedIn: 'root',
})
export class OrganizationService extends EntityService<Organization> {
  constructor(readonly httpClient: HttpClient) {
    super(httpClient, '/organization', Organization.createFromAPI);
  }
}
