import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Entity } from '../../model/entity/entity.model';
import { RequestOptions } from '../../model/request-options.model';
import { ResponsePagination } from '../../model/response-pagination.model';

export class EntityService<T extends Entity> {
  protected url = `${environment.apiUrl}`;

  constructor(
    private readonly http: HttpClient,
    urlFragment: string,
    private createFromAPI: (response: { data: any }) => T
  ) {
    this.url += urlFragment;
  }

  /**
   * Get entity list
   *
   * List can be paginated or un-paginated, depending on the provided request options
   */
  public getList(
    options?: RequestOptions
  ): Observable<T[] | { data: T[]; pagination: ResponsePagination }> {
    const params = {
      ...(options ? options.processForAPI() : {}),
    } as any;

    return this.http
      .get(this.url, { params })
      .pipe(
        map(({ data: response }: any) => {
          if (response?.page && response?.pageSize) {
            const pagination = ResponsePagination.createFromAPI(response);
            const data = (response?.data as any[]).map((responseData) =>
              this.createFromAPI({ data: responseData })
            );

            return { data, pagination };
          }

          return (response as any[]).map((data) =>
            this.createFromAPI({ data })
          );
        })
      );
  }

  /**
   * Create entity
   */
  public create(entity: T): Observable<T> {
    return this.http
      .post(this.url, entity.processForAPI())
      .pipe(map((response: any) => this.createFromAPI(response)));
  }

  /**
   * Get single entity by ID
   */
  public get(entityId: string): Observable<T> {
    return this.http
      .get(`${this.url}/${entityId}`)
      .pipe(map((response: any) => this.createFromAPI(response)));
  }

  /**
   * Update entity
   */
  public update(entity: T): Observable<T> {
    return this.http
      .put(`${this.url}/${entity.id}`, entity.processForAPI())
      .pipe(map((response: any) => this.createFromAPI(response)));
  }

  /**
   * Delete entity
   */
  public delete(entityId: string): Observable<boolean> {
    return this.http
      .delete(`${this.url}/${entityId}`)
      .pipe(map((response: any) => !!response));
  }
}
