import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ActionPopup } from '../model/popup.interface';

@Injectable({
  providedIn: 'root',
})
export class PopupsService {
  // Private event stream sources
  private displayActionPopupSubject = new Subject<ActionPopup>();

  // Public event streams
  public displayActionPopup$ = this.displayActionPopupSubject.asObservable();

  /**
   * Show action popup
   */
  public showActionPopup(popupData: ActionPopup): void {
    this.displayActionPopupSubject.next(popupData);
  }
}
