import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { User } from '../model/user.model';
import { Login } from '../model/login.model';
import { Signup } from '../model/signup.model';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private readonly url = `${environment.apiUrl}/user`;
  constructor(private readonly http: HttpClient) {}

  /**
   * Backend signup request
   * @param signupData - Signup data
   */
  public signup(signupData: Signup): Observable<User> {
    return this.http
      .post(`${this.url}/signup`, signupData.processForAPI())
      .pipe(map((response: any) => User.createFromAPI(response)));
  }

  /**
   * Backend login request
   * @param loginData - Login data
   */
  public login(loginData: Login): Observable<User> {
    return this.http
      .post(`${this.url}/login`, loginData.processForAPI())
      .pipe(map((response: any) => User.createFromAPI(response)));
  }

  /**
   * Get currently authenticated user from backend
   */
  public getCurrentUser(): Observable<User> {
    return this.http
      .get(`${this.url}/current`)
      .pipe(map((response: any) => User.createFromAPI(response)));
  }

  /**
   * Logout currently authenticated user
   */
  public logoutCurrentUser(): Observable<boolean> {
    return this.http
      .get(`${this.url}/logout`)
      .pipe(map((response: any) => !!response));
  }
}
