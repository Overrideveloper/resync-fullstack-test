import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import {
  AuthorizationState,
  AuthorizationStore,
} from '../store/authorization.store';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationGuard implements CanActivate {
  constructor(private readonly authorizationStore: AuthorizationStore) {}

  public canActivate(): boolean {
    // Allow navigation to authentication pages if a user is not currently logged in
    return this.authorizationStore.getAuthorizationState() ===
      AuthorizationState.Authorized
      ? false
      : true;
  }
}
