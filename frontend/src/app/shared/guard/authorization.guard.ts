import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { CanActivate, Router } from '@angular/router';
import { AuthorizationState, AuthorizationStore } from '../store/authorization.store';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationGuard implements CanActivate {
  constructor(
    private readonly authorizationStore: AuthorizationStore,
    private readonly router: Router,
    private readonly location: Location
  ) {}

  canActivate(): boolean | Promise<boolean> {
    // Allow navigation to pages that require authorization if a user is logged in
    if (
      this.authorizationStore.getAuthorizationState() ===
      AuthorizationState.Authorized
    ) {
      return true;
    } else {
      // Redirect to interstitial page to check user logged in state from the server
      // Send route URL as query params to redirect to if user is authenticated
      return this.router.navigate(['interstitial'], {
        queryParams: { redirectTo: this.location.path() },
      });
    }
  }  
}
