import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss'],
})
export class PaginatorComponent {
  @Output() public pageChange = new EventEmitter<number>();
  @Output() public pageBoundsCorrection = new EventEmitter<number>();
}
