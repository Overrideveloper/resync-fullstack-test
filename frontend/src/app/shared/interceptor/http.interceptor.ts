import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, iif, throwError, of } from 'rxjs';
import { concatMap, retryWhen, catchError } from 'rxjs/operators';
import {
  HttpInterceptor as BaseHttpInterceptor,
  HttpEvent,
  HttpHandler,
  HttpRequest,
  HttpErrorResponse,
} from '@angular/common/http';
import {
  AuthorizationState,
  AuthorizationStore,
} from '../store/authorization.store';
import { ToastService } from '../service/toast.service';

@Injectable()
export class HttpInterceptor implements BaseHttpInterceptor {
  constructor(
    private readonly authorizationStore: AuthorizationStore,
    private readonly toastService: ToastService,
    private readonly router: Router
  ) {}

  public intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    req = req.clone({ withCredentials: true });

    return next.handle(req).pipe(
      retryWhen(
        // Retry error-ed requests thrice and only if it is an internal server error
        (errors) =>
          errors.pipe(
            concatMap((error, errorIndex) =>
              iif(
                () =>
                  errorIndex > 3 ||
                  (error instanceof HttpErrorResponse && error.status !== 500),
                throwError(error),
                of(error)
              )
            )
          )
      ),
      catchError((error: any) => {
        // Authorization error
        if (error instanceof HttpErrorResponse && error.status === 401) {
          // Set authorization state
          this.authorizationStore.setAuthorizationState(
            AuthorizationState.Unauthorized
          );
          // Navigate to login age
          this.router.navigate(['auth', 'login']);
        }
        else if (error instanceof HttpErrorResponse && error.status !== 422) {
          // Show error toast
          this.toastService.show(error.error?.message || 'An error occurred. Please try again');
        }
        return throwError(error);
      })
    );
  }
}
