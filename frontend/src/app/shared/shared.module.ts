import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaginatorComponent } from './components/paginator/paginator.component';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [PaginatorComponent],
  imports: [CommonModule, NgxPaginationModule],
  exports: [PaginatorComponent, NgxPaginationModule],
})
export class SharedModule {}
