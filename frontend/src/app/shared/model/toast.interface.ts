import {
  NgbToast,
  NgbToastOptions,
} from '@ng-bootstrap/ng-bootstrap/toast/toast.module';

export interface Toast extends NgbToast {
  className: string;
  text: string;
}

export interface ToastConfig extends NgbToastOptions {
  className: string;
}
