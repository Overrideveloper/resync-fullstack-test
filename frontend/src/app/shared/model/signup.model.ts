import { FormattingUtil } from '../utility/formatting.util';
import { User } from './user.model';

export class Signup extends User {
  public password: string;

  constructor(data: {
    email: string;
    firstName: string;
    lastName: string;
    password: string;
  }) {
    super(data);
    this.password = data?.password ?? '';
  }

  /**
   * Process the login data for the backend request
   */
  public processForAPI(): {
    email: string;
    firstName: string;
    lastName: string;
    password: string;
  } {
    return {
      firstName: this.firstName,
      lastName: this.lastName,
      email: FormattingUtil.stripWhitespaceFromText(this.email),
      password: FormattingUtil.stripWhitespaceFromText(this.password),
    };
  }

  /**
   * Create signup data from form input
   */
  public static createFromForm(formData: any): Signup {
    return new Signup(formData);
  }
}
