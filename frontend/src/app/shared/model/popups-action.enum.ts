export enum PopupsAction {
  Delete = 'delete',
  Cancel = 'cancel',
  Logout = 'logout',
}
