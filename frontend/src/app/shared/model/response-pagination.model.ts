export class ResponsePagination {
  public page: number;
  public pageSize: number;
  public pageCount: number;
  public totalDataCount: number;

  constructor(data?: {
    page: number;
    pageSize: number;
    pageCount: number;
    totalDataCount: number;
  }) {
    this.page = data?.page ?? 1;
    this.pageSize = data?.pageSize ?? 8;
    this.pageCount = data?.pageCount ?? 1;
    this.totalDataCount = data?.totalDataCount ?? 8;
  }

  public static createFromAPI(response: {
    page: number;
    pageSize: number;
    pageCount: number;
    totalDataCount: number;
  }): ResponsePagination {
    return new ResponsePagination(response);
  }
}
