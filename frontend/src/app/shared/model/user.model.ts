export class User {
  public email: string;
  public firstName: string;
  public lastName: string;

  constructor(data: { email: string; firstName: string; lastName: string }) {
    this.email = data?.email ?? '';
    this.firstName = data?.firstName ?? '';
    this.lastName = data?.lastName ?? '';
  }

  /**
   * Create user model instance from backend response
   */
  public static createFromAPI(response: { data: any }): User {
    return new User(response?.data);
  }
}
