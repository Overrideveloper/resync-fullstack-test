export class InsightCount {
  public organization: number;
  public department: number;
  public employee: number;

  constructor(data: {
    organization: number;
    department: number;
    employee: number;
  }) {
    this.organization = data?.organization ?? 0;
    this.department = data?.department ?? 0;
    this.employee = data?.employee ?? 0;
  }

  /**
   * Create insight count model instance from backend response
   */
  public static createFromAPI(response: { data: any }): InsightCount {
    return new InsightCount(response?.data);
  }
}
