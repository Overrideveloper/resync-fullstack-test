import { PopupsAction } from 'src/app/shared/model/popups-action.enum';

export type PopupCallback = (...args: any[]) => void;

export interface ActionPopup {
  action: PopupsAction; // Action to perform
  title: string; // Popup title
  description: string; // Popup description
  actionText: string; // Popup action button text
  actionTextClassName: string; // CSS class name for action text
  callback: PopupCallback; // Callback to be invoked when popup action is dispatched
}
