export enum EntityType {
  Organization = 'organization',
  Department = 'department',
  Employee = 'employee',
}
