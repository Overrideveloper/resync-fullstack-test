import { EntityType } from '../entity-type.enum';

export abstract class Entity {
  public id?: string;
  public type: EntityType;

  constructor(data?: { id?: string; type: EntityType }) {
    this.type = data?.type ?? null as unknown as EntityType;

    if (data?.id) {
      this.id = data.id;
    }
  }

  /**
   * Update entity from form input
   */
  public updateFromForm(formData: any): void {
    Object.entries(formData).forEach(([key, value]) => {
      if (Object.keys(this).includes(key)) {
        (this as any)[key] = value;
      }
    });
  }

  /**
   * Create a deep copy of entity
   * @param withId - Clone with ID field or without
   */
  public clone(withId = true): Entity {
    const entityClone = { ...this };

    if (withId === false) {
      delete entityClone.id;
    }

    return entityClone;
  }

  /**
   * Process entity for backend request
   * @param withId - Process with ID field or without
   */
  public abstract processForAPI(withId?: boolean): any;
}
