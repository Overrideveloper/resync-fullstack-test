import { EntityType } from '../entity-type.enum';
import { Entity } from './entity.model';

export interface ProcessedOrganizationForAPI {
  id?: string;
  name: string;
  address: string;
  city: string;
  state: string;
  country: string;
  owner: string;
}

export class Organization extends Entity {
  public name: string;
  public address: string;
  public city: string;
  public state: string;
  public country: string;
  public owner: string;

  constructor(data: {
    id?: string;
    name: string;
    address: string;
    city: string;
    state: string;
    country: string;
    owner: string;
  }) {
    super({ id: data?.id, type: EntityType.Organization });
    this.name = data?.name ?? '';
    this.address = data?.address ?? '';
    this.city = data?.city ?? '';
    this.state = data?.state ?? '';
    this.country = data?.country ?? '';
    this.owner = data?.owner ?? '';
  }

  /**
   * Process organization instance for backend request
   * @param withId - Process with ID field or without
   */
  public processForAPI(withId = false): ProcessedOrganizationForAPI {
    const payload: ProcessedOrganizationForAPI = {
      id: this.id,
      name: this.name,
      address: this.address,
      city: this.city,
      state: this.state,
      country: this.country,
      owner: this.owner,
    };

    if (withId === false) {
      delete payload.id;
    }

    return payload;
  }

  /**
   * Create a deep copy of organization instance
   * @param withId - Clone with ID field or without
   */
  public clone(withId = true): Organization {
    return new Organization(super.clone(withId) as any);
  }

  /**
   * Create organization model instance from from input
   */
  public static createFromForm(formData: any): Organization {
    return new Organization(formData);
  }

  /**
   * Create organization model instance from backend response
   */
  public static createFromAPI(response: { data: any }): Organization {
    return new Organization(response.data);
  }
}
