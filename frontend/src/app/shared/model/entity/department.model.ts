import { FormattingUtil } from '../../utility/formatting.util';
import { EntityType } from '../entity-type.enum';
import { WorkDay } from '../work-day.enum';
import { WorkTime } from '../work-time.type';
import { Entity } from './entity.model';
import { Organization } from './organization.model';

export interface ProcessedDepartmentForAPI {
  id?: string;
  organizationId?: string;
  name: string;
  description: string;
  owner: string;
  workTimes: WorkTime;
  workDays: WorkDay[];
}

export class Department extends Entity {
  public organization: Organization;
  public name: string;
  public description: string;
  public owner: string;
  public workTimes: WorkTime;
  public workDays: WorkDay[];

  constructor(data: {
    id?: string;
    organization: Organization;
    name: string;
    description: string;
    owner: string;
    workTimes: WorkTime;
    workDays: WorkDay[];
  }) {
    super({ id: data?.id, type: EntityType.Department });
    this.organization = data?.organization ?? null;
    this.name = data?.name ?? '';
    this.description = data?.description ?? '';
    this.owner = data?.owner ?? '';
    this.workTimes = data?.workTimes ?? {};
    this.workDays = data?.workDays ?? '';
  }

  /**
   * Computed workTimeRange property
   */
  public get workTimeRange(): string {
    return FormattingUtil.convertWorkTimeToRangeString(this.workTimes);
  }

  /**
   * Process department instance for backend request
   * @param withId - Process with ID field or without
   */
  public processForAPI(withId = false): ProcessedDepartmentForAPI {
    const payload: ProcessedDepartmentForAPI = {
      id: this.id,
      name: this.name,
      description: this.description,
      owner: this.owner,
      workTimes: this.workTimes,
      workDays: this.workDays,
      organizationId: this.organization?.id ?? '',
    };

    if (withId === false) {
      delete payload.id;
    }

    return payload;
  }

  /**
   * Create a deep copy of department instance
   * @param withId - Clone with ID field or without
   */
  public clone(withId = true): Department {
    return new Department(super.clone(withId) as any);
  }

  /**
   * Create department model instance from from input
   */
  public static createFromForm(formData: any): Department {
    return new Department(formData);
  }

  /**
   * Create department model instance from backend response
   */
  public static createFromAPI(response: { data: any }): Department {
    return new Department(response.data);
  }
}
