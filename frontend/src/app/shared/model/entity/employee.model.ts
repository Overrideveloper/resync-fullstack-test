import { FormattingUtil } from '../../utility/formatting.util';
import { DateOfBirth } from '../date-of-birth.interface';
import { EntityType } from '../entity-type.enum';
import { Department } from './department.model';
import { Entity } from './entity.model';
import { Organization } from './organization.model';

export interface ProcessedEmployeeForAPI {
  id?: string;
  firstName: string;
  lastName: string;
  dob: DateOfBirth;
  workTitle: string;
  totalExperience: number;
  departmentId: string;
}

export class Employee extends Entity {
  public firstName: string;
  public lastName: string;
  public dob: DateOfBirth;
  public workTitle: string;
  public totalExperience: number;
  public department: Department;

  constructor(data: {
    id?: string;
    firstName: string;
    lastName: string;
    dob: DateOfBirth;
    workTitle: string;
    totalExperience: number;
    department: Department;
  }) {
    super({ id: data?.id, type: EntityType.Employee });
    this.firstName = data?.firstName ?? '';
    this.lastName = data?.lastName ?? '';
    this.dob = data?.dob ?? null;
    this.workTitle = data?.workTitle ?? '';
    this.totalExperience = data?.totalExperience ?? 0;
    this.department = data?.department ?? null;
  }

  /**
   * Computed name property
   */
  public get name(): string {
    return `${this.firstName} ${this.lastName}`;
  }

  /**
   * Computed dateOfBirth property
   */
  public get dateOfBirth(): string {
    return FormattingUtil.convertDateOfBirthToString(this.dob);
  }

  /**
   * Computed organization property
   */
  public get organization(): Organization {
    return this.department?.organization;
  }

  /**
   * Process employee instance for backend request
   * @param withId - Process with ID field or without
   */
  public processForAPI(withId = false): ProcessedEmployeeForAPI {
    const payload: ProcessedEmployeeForAPI = {
      id: this.id,
      firstName: this.firstName,
      lastName: this.lastName,
      dob: this.dob,
      workTitle: this.workTitle,
      totalExperience: this.totalExperience,
      departmentId: this.department?.id ?? '',
    };

    if (withId === false) {
      delete payload.id;
    }

    return payload;
  }

  /**
   * Create a deep copy of employee instance
   * @param withId - Clone with ID field or without
   */
  public clone(withId = true): Employee {
    return new Employee(super.clone(withId) as any);
  }

  /**
   * Create employee model instance from from input
   */
  public static createFromForm(formData: any): Employee {
    return new Employee(formData);
  }

  /**
   * Create employee model instance from backend response
   */
  public static createFromAPI(response: { data: any }): Employee {
    return new Employee(response.data);
  }
}
