export class RequestOptions {
  public page!: string;
  public perPage!: string;

  constructor(data?: { page?: string; perPage?: string }) {
    if (data?.page) {
      this.page = data.page;
    }

    if (data?.perPage) {
      this.perPage = data.perPage;
    }
  }

  public processForAPI(): {
    page?: string;
    perPage?: string;
  } {
    let payload: any = {};

    if (this.page) {
      payload.page = this.page;
    }

    if (this.perPage) {
      payload.perPage = this.perPage;
    }

    return payload;
  }

  public static create(data: {
    page?: number;
    perPage?: number;
    omitDeleted?: boolean;
    query?: string;
  }): RequestOptions {
    let page!: string;
    let perPage!: string;

    if (data?.page) {
      page = data.page.toString(10);
    }

    if (data?.perPage) {
      perPage = data.perPage.toString(10);
    }

    return new RequestOptions({
      page,
      perPage,
    });
  }
}
