type WorkTimeType = 'start' | 'end';

interface WorkTimeData {
  hour: number;
  minute: number;
  second: number;
}

export type WorkTime = Record<WorkTimeType, WorkTimeData>;
