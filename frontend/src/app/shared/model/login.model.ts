import { FormattingUtil } from '../utility/formatting.util';

export class Login {
  public email: string;
  public password: string;

  constructor(data: { email: string; password: string }) {
    this.email = data?.email ?? '';
    this.password = data?.password ?? '';
  }

  /**
   * Process the login data for the backend request
   */
  public processForAPI(): {
    email: string;
    password: string;
  } {
    return {
      email: FormattingUtil.stripWhitespaceFromText(this.email),
      password: FormattingUtil.stripWhitespaceFromText(this.password),
    };
  }

  /**
   * Create login data from form input
   */
  public static createFromForm(formData: any): Login {
    return new Login(formData);
  }
}
