import { WorkDay } from './work-day.enum';

export const WorkDayMap = Object.entries(WorkDay).reduce<
  Record<string, string>
>((workDayMap, [key, value]) => {
  workDayMap[value] = key;
  return workDayMap;
}, {});
