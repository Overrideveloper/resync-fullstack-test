export enum WorkDay {
	Monday = 'MON',
	Tuesday = 'TUE',
	Wednesday = 'WED',
	Thursday = 'THUR',
	Friday = 'FRI',
	Saturday = 'SAT',
	Sunday = 'SUN',
}
