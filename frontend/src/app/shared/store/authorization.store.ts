import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '../model/user.model';

export enum AuthorizationState {
  Authorized = 'authorized',
  Unauthorized = 'unauthorized',
}

@Injectable({
  providedIn: 'root',
})
export class AuthorizationStore {
  private dataStore = {
    authorizationState: AuthorizationState.Unauthorized,
    user: (null as unknown) as User,
  };

  private authorizationStateSubject = new BehaviorSubject<AuthorizationState>(
    this.dataStore.authorizationState
  );
  private userSubject = new BehaviorSubject<User>(this.dataStore.user);

  public readonly authorizationState$: Observable<AuthorizationState> = this.authorizationStateSubject.asObservable();
  public readonly user$: Observable<User> = this.userSubject.asObservable();

  public getAuthorizationState(): AuthorizationState {
    return this.dataStore.authorizationState;
  }

  public setAuthorizationState(authorizationState: AuthorizationState) {
    this.dataStore.authorizationState = authorizationState;
    this.authorizationStateSubject.next(this.dataStore.authorizationState);
  }

  public setUser(user: User) {
    this.dataStore.user = user;
    this.userSubject.next(this.dataStore.user);
  }
}
