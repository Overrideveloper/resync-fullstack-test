import { TestBed } from '@angular/core/testing';

import { AuthorizationStore } from './authorization.store';

describe('AuthorizationStore', () => {
  let service: AuthorizationStore;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthorizationStore);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
