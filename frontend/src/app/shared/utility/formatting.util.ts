import { DateOfBirth } from '../model/date-of-birth.interface';
import { WorkTime } from '../model/work-time.type';

export class FormattingUtil {
  /**
   * Add 0 to beginning of number values less than 10
   */
  private static padNumberValue(value: number): string {
    return value < 10 ? `0${value}` : `${value}`;
  }

  public static stripWhitespaceFromText(text: string): string {
    return text.replace(/ /g, '');
  }

  public static convertWorkTimeToRangeString(workTime: WorkTime): string {
    const pad = this.padNumberValue;
    const start = workTime.start;
    const end = workTime.end;

    return `${pad(start.hour)}:${pad(start.minute)} to ${pad(end.hour)}:${pad(
      end.minute
    )}`;
  }

  public static convertDateOfBirthToString(dob: DateOfBirth): string {
    const pad = this.padNumberValue;

    return `${pad(dob.day)}/${pad(dob.month)}/${pad(dob.year)}`;
  }
}
