import { FormGroup, ValidationErrors } from '@angular/forms';

export class CustomFormValidator {
  /**
   * Custom validator for password & confirm password values
   */
  public static confirmPassword(formGroup: FormGroup): ValidationErrors | null {
    const password = formGroup.get('password');
    const confirmPassword = formGroup.get('confirmPassword');

    // Passwords do not match
    if (password?.value !== confirmPassword?.value) {
      // Return validation error
      return { passwordMismatch: true };
    }

    return null;
  }

  /**
   * Custom validator for work times
   */
  public static validateWorkTimes(
    formGroup: FormGroup
  ): ValidationErrors | null {
    const startWorkTime = formGroup.get('startWorkTime');
    const endWorkTime = formGroup.get('endWorkTime');

    // startWorkTime is greater than endWorkTime
    if (
      startWorkTime?.value.hour > endWorkTime?.value.hour ||
      (startWorkTime?.value.hour === endWorkTime?.value.hour &&
        startWorkTime?.value.minute > endWorkTime?.value.minute)
    ) {
      // Return validation error
      return { invalidTimeRange: true };
    }

    // startWorkTime equals endWorkTime
    if (
      startWorkTime?.value.hour === endWorkTime?.value.hour &&
      startWorkTime?.value.minute === endWorkTime?.value.minute
    ) {
      // Return validation error
      return { invalidTimeRange: true };
    }

    return null;
  }
}
