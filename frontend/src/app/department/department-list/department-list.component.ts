import { Component, OnInit } from '@angular/core';
import { PaginationInstance } from 'ngx-pagination';
import { BehaviorSubject, Observable } from 'rxjs';
import { Department } from 'src/app/shared/model/entity/department.model';
import { PopupsAction } from 'src/app/shared/model/popups-action.enum';
import { RequestOptions } from 'src/app/shared/model/request-options.model';
import { ResponsePagination } from 'src/app/shared/model/response-pagination.model';
import { WorkDayMap } from 'src/app/shared/model/work-day.map';
import { DepartmentService } from 'src/app/shared/service/entity/department.service';
import { PopupsService } from 'src/app/shared/service/popups.service';
import { ToastService } from 'src/app/shared/service/toast.service';

interface DepartmentListResponse {
  data: Department[];
  pagination: ResponsePagination;
}

@Component({
  selector: 'app-department-list',
  templateUrl: './department-list.component.html',
  styleUrls: []
})
export class DepartmentListComponent implements OnInit {
  public departmentList!: Department[];
  public pagination!: PaginationInstance;
  public page = 1;
  public WorkDayMap = WorkDayMap;

  // Loading state control
  private loadingSubject = new BehaviorSubject<boolean>(true);
  public readonly loading$: Observable<boolean> = this.loadingSubject.asObservable();

  constructor(
    private readonly departmentService: DepartmentService,
    private readonly toastService: ToastService,
    private readonly popupsService: PopupsService
  ) {}

  public ngOnInit(): void {
    this.loadDepartmentList();
  }

  /**
   * Handle pagination page change
   */
  public handlePageChange(page: number): void {
    this.page = page;
    this.loadDepartmentList();
  }

  /**
   * Open delete confirmation popup
   */
  public openDeleteConfirmationPopup(department: Department) {
    this.popupsService.showActionPopup({
      action: PopupsAction.Delete,
      actionText: 'Delete',
      actionTextClassName: 'btn btn-danger',
      callback: this.deleteDepartment.bind(this, department),
      title: `Delete department "${department.name}"`,
      description: 'Are you sure you want to delete this department?',
    });
  }

  /**
   * Delete selected department
   */
  private deleteDepartment(department: Department) {
    this.departmentService.delete(department.id as string).subscribe(() => {
      // Show success toast
      this.toastService.show(`Department "${department.name}" deleted`, {
        className: 'bg-primary text-light opacity-4',
        delay: 3000,
      });
      // Reload list
      this.loadDepartmentList();
    });
  }

  /**
   * Load department list
   */
  private loadDepartmentList(): void {
    const requestOptions = RequestOptions.create({
      page: this.page,
      perPage: 8,
    });

    this.loadingSubject.next(true);

    this.departmentService.getList(requestOptions).subscribe((result) => {
      const { pagination, data } = result as DepartmentListResponse;
      this.pagination = {
        itemsPerPage: pagination.pageSize,
        currentPage: pagination.page,
        totalItems: pagination.totalDataCount,
      };

      this.departmentList = data;
      this.loadingSubject.next(false);
    });
  }
}
