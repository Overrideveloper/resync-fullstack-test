import { LocationStrategy } from '@angular/common';
import { Component, OnDestroy } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Department } from 'src/app/shared/model/entity/department.model';
import { Organization } from 'src/app/shared/model/entity/organization.model';
import { WorkDay } from 'src/app/shared/model/work-day.enum';
import { WorkTime } from 'src/app/shared/model/work-time.type';
import { DepartmentService } from 'src/app/shared/service/entity/department.service';
import { OrganizationService } from 'src/app/shared/service/entity/organization.service';
import { ToastService } from 'src/app/shared/service/toast.service';
import { CustomFormValidator } from 'src/app/shared/utility/validator.util';

@Component({
  selector: 'app-create-edit-department',
  templateUrl: './create-edit-department.component.html',
  styleUrls: [],
})
export class CreateEditDepartmentComponent implements OnDestroy {
  public mode!: 'create' | 'edit';
  public department!: Department;
  public organizationList!: Organization[];
  public workDayList = Object.entries(WorkDay);
  public submitted = false;
  public createEditForm!: FormGroup;

  private activatedRouteSubscription!: Subscription;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly departmentService: DepartmentService,
    private readonly organizationService: OrganizationService,
    private readonly router: Router,
    private readonly locationStrategy: LocationStrategy,
    private readonly activatedRoute: ActivatedRoute,
    private readonly toastService: ToastService
  ) {
    this.listenToRouteParams();
  }

  public ngOnDestroy(): void {
    this.activatedRouteSubscription?.unsubscribe();
  }

  /**
   * Get form control objects
   */
  public get controls(): Record<string, AbstractControl> {
    return this.createEditForm.controls;
  }

  /**
   * Setup form with provided department (edit mode) or empty values (create mode)
   */
  public setupForm(department?: Department): void {
    this.createEditForm = this.formBuilder.group(
      {
        name: [department?.name ?? '', Validators.required],
        description: [department?.description ?? '', Validators.required],
        owner: [department?.owner ?? '', Validators.required],
        startWorkTime: [
          department?.workTimes.start ?? { hour: 0, minute: 0 },
          Validators.required,
        ],
        endWorkTime: [
          department?.workTimes.end ?? { hour: 0, minute: 0 },
          Validators.required,
        ],
        organizationId: [
          department?.organization.id ?? '',
          Validators.required,
        ],
        workDays: [
          department?.workDays ?? [],
          Validators.compose([Validators.required, Validators.minLength(1)]),
        ],
      },
      {
        validators: [CustomFormValidator.validateWorkTimes],
      }
    );
  }

  /**
   * Submit form data and make request to backend
   */
  public submitForm() {
    if (this.createEditForm.valid === true) {
      // Trigger form submitting UI state
      this.submitted = true;

      switch (this.mode) {
        case 'create':
          this.submitCreateForm();
          break;

        case 'edit':
          this.submitEditForm();
          break;

        default:
          throw new Error(`Case "${this.mode}" not yet implemented`);
      }
    }
  }

  /**
   * Submit form for create mode
   */
  private submitCreateForm(): void {
    const workTimes: WorkTime = {
      start: this.createEditForm.value.startWorkTime,
      end: this.createEditForm.value.endWorkTime,
    };

    const organization = this.organizationList.find(
      (organization) =>
        organization.id === this.createEditForm.value.organizationId
    );

    const departmentToCreate = Department.createFromForm({
      ...this.createEditForm.value,
      workTimes,
      organization,
    });

    this.departmentService.create(departmentToCreate).subscribe(
      (department) => {
        // Display success toast and navigate to list page
        this.toastService.show(`Department "${department.name}" created.`, {
          className: 'bg-primary text-light opacity-4',
          delay: 3000,
        });
        this.router.navigate(['departments']);
      },
      // Trigger form default UI state
      (_) => (this.submitted = false)
    );
  }

  /**
   * Submit form for edit mode
   */
  private submitEditForm(): void {
    const departmentToEdit = this.department.clone();
    const workTimes: WorkTime = {
      start: this.createEditForm.value.startWorkTime,
      end: this.createEditForm.value.endWorkTime,
    };

    departmentToEdit.updateFromForm({
      ...this.createEditForm.value,
      workTimes,
    });

    this.departmentService.update(departmentToEdit).subscribe(
      (department) => {
        // Display success toast and navigate to list page
        this.toastService.show(`Department "${department.name}" updated.`, {
          className: 'bg-primary text-light opacity-4',
          delay: 3000,
        });
        this.router.navigate(['departments']);
      },
      // Trigger form default UI state
      (_) => (this.submitted = false)
    );
  }

  /**
   * Listen to route params to determine waht form mode to setup, create or edit
   */
  private listenToRouteParams(): void {
    const routePath = this.locationStrategy.path();

    // Route is "new", setup create form
    if (routePath.includes('new')) {
      this.mode = 'create';
      this.organizationService.getList().subscribe((organizationList) => {
        this.organizationList = organizationList as Organization[];
        this.setupForm();
      });
    }

    // Route is "edit", load department and setup edit form
    if (routePath.includes('edit')) {
      this.activatedRouteSubscription = this.activatedRoute.paramMap.subscribe(
        (params) => {
          const departmentId = params.get('departmentId');

          if (departmentId) {
            this.mode = 'edit';
            this.departmentService.get(departmentId).subscribe((department) => {
              this.department = department;
              this.setupForm(department);
            });
          }
        }
      );
    }
  }
}
