import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DepartmentRoutingModule } from './department-routing.module';
import { DepartmentComponent } from './department.component';
import { DepartmentListComponent } from './department-list/department-list.component';
import { CreateEditDepartmentComponent } from './create-edit-department/create-edit-department.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { NgxSelectModule } from 'ngx-select-ex';
import { NgbTimepickerModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    DepartmentComponent,
    DepartmentListComponent,
    CreateEditDepartmentComponent,
  ],
  imports: [
    CommonModule,
    DepartmentRoutingModule,
    ReactiveFormsModule,
    NgxSelectModule,
    SharedModule,
    NgbTimepickerModule,
  ],
})
export class DepartmentModule {}
