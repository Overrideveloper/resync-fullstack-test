import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateEditDepartmentComponent } from './create-edit-department/create-edit-department.component';
import { DepartmentListComponent } from './department-list/department-list.component';
import { DepartmentComponent } from './department.component';

const routes: Routes = [
  {
    path: '',
    component: DepartmentComponent,
    children: [
      {
        path: '',
        component: DepartmentListComponent,
      },
      {
        path: 'new',
        component: CreateEditDepartmentComponent,
      },
      {
        path: 'edit/:departmentId',
        component: CreateEditDepartmentComponent,
      },
      {
        path: '**',
        redirectTo: '',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DepartmentRoutingModule {}
