import { Component } from '@angular/core';
import {
  AuthorizationState,
  AuthorizationStore,
} from './shared/store/authorization.store';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  public authorizationState$ = this.authorizationStore.authorizationState$;
  public AuthorizationState = AuthorizationState;

  constructor(private readonly authorizationStore: AuthorizationStore) {}
}
