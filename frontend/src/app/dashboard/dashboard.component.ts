import { Component, OnInit } from '@angular/core';
import { InsightCount } from '../shared/model/insight-count.model';
import { InsightService } from '../shared/service/insight.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [],
})
export class DashboardComponent implements OnInit {
  public insightCount!: InsightCount;
  constructor(private readonly insightService: InsightService) {}

  public ngOnInit(): void {
    this.loadEntityInsightCount();
  }

  /**
   * Load entity insight count
   */
  private loadEntityInsightCount(): void {
    this.insightService
      .getEntitiesCount()
      .subscribe((insightCount) => (this.insightCount = insightCount));
  }
}
