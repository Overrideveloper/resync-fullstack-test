import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationGuard } from './shared/guard/authentication.guard';
import { AuthorizationGuard } from './shared/guard/authorization.guard';

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: () =>
      import('./authentication/authentication.module').then(
        (m) => m.AuthenticationModule
      ),
    canActivate: [AuthenticationGuard],
  },
  {
    path: 'interstitial',
    loadChildren: () =>
      import('./interstitial/interstitial.module').then(
        (m) => m.InterstitialModule
      ),
  },
  {
    path: 'dashboard',
    loadChildren: () =>
      import('./dashboard/dashboard.module').then((m) => m.DashboardModule),
    canActivate: [AuthorizationGuard],
  },
  {
    path: 'organizations',
    loadChildren: () =>
      import('./organization/organization.module').then(
        (m) => m.OrganizationModule
      ),
    canActivate: [AuthorizationGuard],
  },
  {
    path: 'departments',
    loadChildren: () =>
      import('./department/department.module').then((m) => m.DepartmentModule),
    canActivate: [AuthorizationGuard],
  },
  {
    path: 'employees',
    loadChildren: () =>
      import('./employee/employee.module').then((m) => m.EmployeeModule),
    canActivate: [AuthorizationGuard],
  },
  {
    path: 'calendar',
    loadChildren: () =>
      import('./calendar/calendar.module').then((m) => m.CalendarModule),
    canActivate: [AuthorizationGuard],
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'dashboard',
  },
  {
    path: '**',
    redirectTo: 'dashboard',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
