import { Component, OnInit } from '@angular/core';
import { PaginationInstance } from 'ngx-pagination';
import { BehaviorSubject, Observable } from 'rxjs';
import { Employee } from 'src/app/shared/model/entity/employee.model';
import { PopupsAction } from 'src/app/shared/model/popups-action.enum';
import { RequestOptions } from 'src/app/shared/model/request-options.model';
import { ResponsePagination } from 'src/app/shared/model/response-pagination.model';
import { EmployeeService } from 'src/app/shared/service/entity/employee.service';
import { PopupsService } from 'src/app/shared/service/popups.service';
import { ToastService } from 'src/app/shared/service/toast.service';

interface EmployeeListResponse {
  data: Employee[];
  pagination: ResponsePagination;
}

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: [],
})
export class EmployeeListComponent implements OnInit {
  public employeeList!: Employee[];
  public pagination!: PaginationInstance;
  public page = 1;

  // Loading state control
  private loadingSubject = new BehaviorSubject<boolean>(true);
  public readonly loading$: Observable<boolean> = this.loadingSubject.asObservable();

  constructor(
    private readonly employeeService: EmployeeService,
    private readonly toastService: ToastService,
    private readonly popupsService: PopupsService
  ) {}

  public ngOnInit(): void {
    this.loadEmployeeList();
  }

  /**
   * Handle pagination page change
   */
  public handlePageChange(page: number): void {
    this.page = page;
    this.loadEmployeeList();
  }

  /**
   * Open delete confirmation popup
   */
  public openDeleteConfirmationPopup(employee: Employee) {
    this.popupsService.showActionPopup({
      action: PopupsAction.Delete,
      actionText: 'Delete',
      actionTextClassName: 'btn btn-danger',
      callback: this.deleteEmployee.bind(this, employee),
      title: `Delete employee "${employee.name}"`,
      description: 'Are you sure you want to delete this employee?',
    });
  }

  /**
   * Delete selected employee
   */
  private deleteEmployee(employee: Employee) {
    this.employeeService.delete(employee.id as string).subscribe(() => {
      // Show success toast
      this.toastService.show(`Employee "${employee.name}" deleted`, {
        className: 'bg-primary text-light opacity-4',
        delay: 3000,
      });
      // Reload list
      this.loadEmployeeList();
    });
  }

  /**
   * Load employee list
   */
  private loadEmployeeList(): void {
    const requestOptions = RequestOptions.create({
      page: this.page,
      perPage: 8,
    });

    this.loadingSubject.next(true);

    this.employeeService.getList(requestOptions).subscribe((result) => {
      const { pagination, data } = result as EmployeeListResponse;
      this.pagination = {
        itemsPerPage: pagination.pageSize,
        currentPage: pagination.page,
        totalItems: pagination.totalDataCount,
      };

      this.employeeList = data;
      this.loadingSubject.next(false);
    });
  }
}
