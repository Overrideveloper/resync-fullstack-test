import { LocationStrategy } from '@angular/common';
import { Component, OnDestroy } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { forkJoin, Subscription } from 'rxjs';
import { Organization } from 'src/app/shared/model/entity/organization.model';
import { Department } from 'src/app/shared/model/entity/department.model';
import { Employee } from 'src/app/shared/model/entity/employee.model';
import { OrganizationService } from 'src/app/shared/service/entity/organization.service';
import { DepartmentService } from 'src/app/shared/service/entity/department.service';
import { EmployeeService } from 'src/app/shared/service/entity/employee.service';
import { ToastService } from 'src/app/shared/service/toast.service';

@Component({
  selector: 'app-create-edit-employee',
  templateUrl: './create-edit-employee.component.html',
  styleUrls: [],
})
export class CreateEditEmployeeComponent implements OnDestroy {
  public mode!: 'create' | 'edit';
  public employee!: Employee;
  public submitted = false;
  public createEditForm!: FormGroup;
  public organizationDepartmentList!: Department[];
  public organizationList!: Organization[];
  public organizationFormControl = new FormControl();

  private departmentList!: Department[];
  private subscriptionList: Subscription[] = [];

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly organizationService: OrganizationService,
    private readonly departmentService: DepartmentService,
    private readonly employeeService: EmployeeService,
    private readonly router: Router,
    private readonly locationStrategy: LocationStrategy,
    private readonly activatedRoute: ActivatedRoute,
    private readonly toastService: ToastService
  ) {
    this.listenToRouteParams();
  }

  public ngOnDestroy(): void {
    this.subscriptionList.forEach((subscription) => subscription.unsubscribe());
  }

  /**
   * Get form control objects
   */
  public get controls(): Record<string, AbstractControl> {
    return this.createEditForm.controls;
  }

  /**
   * Setup form with provided employee (edit mode) or empty values (create mode)
   */
  public setupForm(employee?: Employee): void {
    // Load organization and department list
    forkJoin([
      this.organizationService.getList(),
      this.departmentService.getList(),
    ]).subscribe(([organizationList, departmentList]) => {
      // Setup organization & department lists
      this.organizationList = organizationList as Organization[];
      this.departmentList = departmentList as Department[];
      this.organizationFormControl = new FormControl(
        employee?.department.organization.id
      );
      this.organizationDepartmentList = this.departmentList.filter(
        (department) =>
          department.organization.id === employee?.department.organization.id
      );

      // Setup form
      this.createEditForm = this.formBuilder.group({
        firstName: [employee?.firstName ?? '', Validators.required],
        lastName: [employee?.lastName ?? '', Validators.required],
        dob: [employee?.dob ?? null, Validators.required],
        workTitle: [employee?.workTitle ?? '', Validators.required],
        totalExperience: [employee?.totalExperience ?? 0, Validators.required],
        departmentId: [employee?.department.id ?? '', Validators.required],
      });

      this.handleOrganizationChange();
    });
  }

  /**
   * Handle organization value change
   *
   * - Populate department select with organization's departments
   * - Reset departmentId form control
   */
  public handleOrganizationChange(): void {
    this.subscriptionList.push(
      this.organizationFormControl.valueChanges.subscribe((value: string) => {
        this.controls.departmentId.reset();
        this.organizationDepartmentList = this.departmentList.filter(
          (department) => department.organization.id === value
        );
      })
    );
  }

  /**
   * Submit form data and make request to backend
   */
  public submitForm() {
    if (this.createEditForm.valid === true) {
      // Trigger form submitting UI state
      this.submitted = true;

      switch (this.mode) {
        case 'create':
          this.submitCreateForm();
          break;

        case 'edit':
          this.submitEditForm();
          break;

        default:
          throw new Error(`Case "${this.mode}" not yet implemented`);
      }
    }
  }

  /**
   * Submit form for create mode
   */
  private submitCreateForm(): void {
    const department = this.departmentList.find(
      (department) => department.id === this.createEditForm.value.departmentId
    );
    const employeeToCreate = Employee.createFromForm({
      ...this.createEditForm.value,
      department,
    });

    this.employeeService.create(employeeToCreate).subscribe(
      (employee) => {
        // Display success toast and navigate to list page
        this.toastService.show(`Employee "${employee.name}" created.`, {
          className: 'bg-primary text-light opacity-4',
          delay: 3000,
        });
        this.router.navigate(['employees']);
      },
      // Trigger form default UI state
      (_) => (this.submitted = false)
    );
  }

  /**
   * Submit form for edit mode
   */
  private submitEditForm(): void {
    const employeeToEdit = this.employee.clone();
    const department = this.departmentList.find(
      (department) => department.id === this.createEditForm.value.departmentId
    );
    employeeToEdit.updateFromForm({
      ...this.createEditForm.value,
      department,
    });

    this.employeeService.update(employeeToEdit).subscribe(
      (employee) => {
        // Display success toast and navigate to list page
        this.toastService.show(`Employee "${employee.name}" updated.`, {
          className: 'bg-primary text-light opacity-4',
          delay: 3000,
        });
        this.router.navigate(['employees']);
      },
      // Trigger form default UI state
      (_) => (this.submitted = false)
    );
  }

  /**
   * Listen to route params to determine waht form mode to setup, create or edit
   */
  private listenToRouteParams(): void {
    const routePath = this.locationStrategy.path();

    // Route is "new", setup create form
    if (routePath.includes('new')) {
      this.mode = 'create';
      this.setupForm();
    }

    // Route is "edit", load organization and setup edit form
    if (routePath.includes('edit')) {
      this.subscriptionList.push(
        this.activatedRoute.paramMap.subscribe((params) => {
          const employeeId = params.get('employeeId');

          if (employeeId) {
            this.mode = 'edit';
            this.employeeService.get(employeeId).subscribe((employee) => {
              this.employee = employee;
              this.setupForm(employee);
            });
          }
        })
      );
    }
  }
}
